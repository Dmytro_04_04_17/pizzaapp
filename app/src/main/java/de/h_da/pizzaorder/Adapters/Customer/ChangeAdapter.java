package de.h_da.pizzaorder.Adapters.Customer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.R;

/**
 * Adapter for editing menus added for customer
 */
public class ChangeAdapter extends RecyclerView.Adapter<ChangeAdapter.ChangeHolder> {

    // Data with menus added to a basket
    private List<MenuDB> listData;

    private List<Boolean> checkboxList;

    public ChangeAdapter(List<MenuDB> list, List<Boolean> tempList) {
        this.listData = list;
        this.checkboxList = tempList;
    }

    @Override
    public ChangeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row_change, parent, false);
        return new ChangeHolder(view);
    }

    @Override
    public void onBindViewHolder(ChangeHolder holder, int position) {
        holder.nameTextView.setText(this.listData.get(position).getName());
        holder.checkBox.setChecked(this.checkboxList.get(position));
    }

    @Override
    public int getItemCount() {
        return this.listData.size();
    }

    class ChangeHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;

        private CheckBox checkBox;

        public ChangeHolder(View itemView) {
            super(itemView);
            this.nameTextView = (TextView) itemView.findViewById(R.id.RecyclerRowChange_Menu_TextView);
            this.checkBox = (CheckBox) itemView.findViewById(R.id.RecyclerRowChange_Checkbox);
            this.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        checkboxList.set(getAdapterPosition(), true);
                    } else {
                        checkboxList.set(getAdapterPosition(), false);
                    }
                }
            });
        }
    }
}
