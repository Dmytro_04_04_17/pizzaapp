package de.h_da.pizzaorder.Adapters.Chef;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.R;

/**
 * Adapter for displaying orders for chefs
 */
public class ChefAllAdapter extends RecyclerView.Adapter<ChefAllAdapter.Holder> {

    // list with all orders available
    private List<MenuDB> allList;

    // list with selected orders
    private List<Boolean> selectedList;

    // Reference to the activity
    private Communicator comm;

    public ChefAllAdapter(List<MenuDB> list, Context context) {
        this.allList = list;
        this.selectedList = new ArrayList<>(Collections.nCopies(this.allList.size(), false));
        this.comm = (Communicator)context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row_chef_all, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.textView.setText(this.allList.get(position).getName());
        if(this.selectedList.get(position)) {
            holder.textView.setBackgroundResource(R.color.colorMenuHighlight);
        } else {
            holder.textView.setBackgroundResource(R.color.colorTransparent);

        }
    }

    @Override
    public int getItemCount() {
        return this.allList.size();
    }

    class Holder extends RecyclerView.ViewHolder{
        // For displaying the name of the Menu to be cooked
        TextView textView;

        View view;

        public Holder(View itemView) {
            super(itemView);
            this.textView = (TextView)itemView.findViewById(R.id.RecyclerRowChefAll_Text_TextView);
            this.view = itemView;
            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    comm.respondChefAllAdapter(getAdapterPosition());
                    addValueForColor(getAdapterPosition());
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }

        private void addValueForColor(int position) {
            if(selectedList.get(position)) {
                selectedList.set(position, false);
            } else {
                selectedList.set(position, true);
            }
        }
    }
    public interface Communicator {
        void respondChefAllAdapter(int id);
    }
}
