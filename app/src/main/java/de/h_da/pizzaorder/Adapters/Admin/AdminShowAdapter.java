package de.h_da.pizzaorder.Adapters.Admin;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;

public class AdminShowAdapter extends RecyclerView.Adapter<AdminShowAdapter.Holder>{

    private List listData;

    private int type;

    private Communicator comm;

    public AdminShowAdapter(Context context, int type, List data) {
        this.comm = (Communicator) context;
        this.type = type;
        this.listData = data;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row_admin_adapter, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.positionTextView.setText(String.valueOf(position + 1));
        String name;
        if (this.type == 0) {
            name = ((List<UserDB>) this.listData).get(position).getUsername();
        } else if (this.type == 1) {
            name = ((List<MenuDB>) this.listData).get(position).getName();
            holder.optionalTextView.setVisibility(View.VISIBLE);
            if (((List<MenuDB>) this.listData).get(position).getActive() != 0) {
                holder.optionalTextView.setVisibility(View.VISIBLE);
                holder.optionalTextView.setText("\u2713");
            }
        } else {
            String temp = ((List<OrderDB>) this.listData).get(position).getDate().substring(0, 10).replace("-", ".");
            name = temp.substring(8,10) + "." + temp.substring(5,7) + "." + temp.substring(0,4);
        }
        holder.nameTextView.setText(name);
    }

    @Override
    public int getItemCount() {
        return this.listData.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        // For displaying the actual number
        TextView positionTextView;
        // For displaying the name
        TextView nameTextView;
        // For displaying whether this Menu is active or not
        TextView optionalTextView;

        public Holder(View itemView) {
            super(itemView);
            this.positionTextView = (TextView)itemView.findViewById(R.id.RecyclerRow_AdminShow_Number_TextView);
            this.nameTextView = (TextView)itemView.findViewById(R.id.RecyclerRow_AdminShow_Name_TextView);
            this.optionalTextView = (TextView)itemView.findViewById(R.id.RecyclerRow_AdminShow_Optional_TextView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (comm != null) {
                        comm.respondAdminShowAdapter(getAdapterPosition());
                    }
                }
            });
        }
    }
    public interface Communicator {
        void respondAdminShowAdapter(int position);
    }
}
