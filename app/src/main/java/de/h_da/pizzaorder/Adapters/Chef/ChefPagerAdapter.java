package de.h_da.pizzaorder.Adapters.Chef;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import de.h_da.pizzaorder.Fragments.Chef.ChefAllOrdersFragment;
import de.h_da.pizzaorder.Models.MenuDB;

public class ChefPagerAdapter extends FragmentStatePagerAdapter {

    private List<MenuDB> menuList;

    private List<MenuDB> ownList;

    private Context context;

    public ChefPagerAdapter(Context context, FragmentManager fm, List<MenuDB> menuList, List<MenuDB> ownList) {
        super(fm);
        this.context = context;
        this.menuList = menuList;
        this.ownList = ownList;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return ChefAllOrdersFragment.newInstance((ArrayList<MenuDB>)this.menuList, true);
            case 1:
                return ChefAllOrdersFragment.newInstance((ArrayList<MenuDB>)this.ownList, false);
            default:
                throw new IllegalStateException("Unexpected value");
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
