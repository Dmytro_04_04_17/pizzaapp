package de.h_da.pizzaorder.Adapters.Customer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import de.h_da.pizzaorder.R;

/**
 * Adapter for displaying info from settings activity
 */
public class SettingsAdapter extends BaseAdapter {

    private List<String> data;

    public SettingsAdapter(List<String> data)
    {
        this.data = data;
    }

    @Override
    public int getCount() {
        return this.data.size();
    }

    @Override
    public String getItem(int position) {
        return this.data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        SettingsListHolder holder;

        if(view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview_row_settings, parent, false);
            holder = new SettingsListHolder(view);
            view.setTag(holder);
        } else {
            holder = (SettingsListHolder) view.getTag();
        }

        holder.dataTextView.setText(data.get(position));

        if(position % 2 != 0) {
            holder.dividerView.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private class SettingsListHolder {

        private TextView dataTextView;

        private View dividerView;

        private SettingsListHolder(View itemView) {
            this.dataTextView = (TextView)itemView.findViewById(R.id.RecyclerRow_Data_TextView);
            this.dividerView = itemView.findViewById(R.id.RecyclerRow_Divider_View);
        }
    }
}
