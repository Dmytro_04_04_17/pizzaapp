package de.h_da.pizzaorder.Adapters.Delivery;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;

public class AllOrdersAdapter extends RecyclerView.Adapter<AllOrdersAdapter.OrderHolder> {

    //Member variables
    private HashMap<String, ArrayList<OrderDB>> mAllReadyOrders;
    private ArrayList<String> mOrdersTimes;

    private Communicator mComm;
    private Context mContext;


    /*
    * @param allorders these are just the ready orders
    * */
    public AllOrdersAdapter(HashMap<String, ArrayList<OrderDB>> readyorders, Context context) {
        this.mAllReadyOrders = readyorders;
        this.mOrdersTimes = new ArrayList<>(mAllReadyOrders.keySet());
        this.mContext = context;
        this.mComm = (Communicator) context;

    }


    @Override
    public AllOrdersAdapter.OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_row_all_orders_row, parent, false);
        return new OrderHolder(orderView, mContext);
    }

    @Override
    public void onBindViewHolder(AllOrdersAdapter.OrderHolder holder, int position) {

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        Date now = new Date();
        Date orderDate = new Date();
        SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
        try {
            orderDate = dates.parse(mOrdersTimes.get(position));
        } catch (ParseException e) {
            Log.d("xxxx", "error in converting date");
            e.printStackTrace();
        }
        long diff = now.getTime() - orderDate.getTime();
        //TODO should show the name of the user
        // mOrdersTimes.get(position) : time of the order
        String text = "The order is submitted at : " + df.format("HH:mm", orderDate);
        text += "\norder contains : "
                + mAllReadyOrders.get(mOrdersTimes.get(position)).size()
                + " item"
                + "\n\n"
                + String.valueOf(diff / 60000)
                + " min is passed from the order time";

        if ((diff / 60000) > 30) {
            holder.tvOrderName.setBackgroundResource(R.color.colorOverDueOrder);
        } else {
            holder.tvOrderName.setBackgroundResource(R.color.colorNormalOrder);
        }
        holder.tvOrderName.setText(text);

    }

    @Override
    public int getItemCount() {
        return mAllReadyOrders.size();
    }

    public interface Communicator {
        void respondOneOrderSelected(String selectedOrderTime, ArrayList<OrderDB> itemsOfSelectedOrder);
    }

    public class OrderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvOrderName;
        Context context;

        public OrderHolder(View v, Context c) {
            super(v);
            context = c;
            tvOrderName = (TextView) v.findViewById(R.id.RecyclerRow_All_Orders_Order_Name);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mComm != null) {
                /*
                * send the selected orderId to Delivery Activity to open order description fragment
                * */
                String time = mOrdersTimes.get(getAdapterPosition());
                ArrayList<OrderDB> itemsOfOrder = mAllReadyOrders.get(time);
                mComm.respondOneOrderSelected(time, itemsOfOrder);
            }
        }
    }
}
