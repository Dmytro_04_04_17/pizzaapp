package de.h_da.pizzaorder.Adapters.Customer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;

/**
 * Adadpter for displaying orders which user added previously
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {

    private List<OrderDB> listOrder;

    private Communicator comm;

    private int selectedItem = -1;

    private int previousSelected = -1;

    public HistoryAdapter(Context context, List<OrderDB> list) {
        this.comm = (Communicator) context;
        this.listOrder = list;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row_history, parent, false);
        return new HistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        holder.statusTextView.setText(this.listOrder.get(position).getStatus());

        String receiveDate = this.listOrder.get(position).getDate().substring(0, 10).replace("-", ".");
        String formattedDate = receiveDate.substring(8,10) + "." + receiveDate.substring(5,7) + "." + receiveDate.substring(0,4);
        holder.dateTextView.setText(formattedDate);

        String status = this.listOrder.get(position).getStatus();
        String feedback = this.listOrder.get(position).getCustomerFeedback();
        if (status.equals("delivered") && feedback.equals("")) {
            holder.imageView.setImageResource(android.R.drawable.btn_star_big_off);
            holder.imageView.setVisibility(View.VISIBLE);
        } else if (status.equals("delivered") && feedback.length() > 0) {
            holder.imageView.setImageResource(android.R.drawable.btn_star_big_on);
            holder.imageView.setVisibility(View.VISIBLE);
        } else {
            holder.imageView.setVisibility(View.INVISIBLE);
        }

        if(position == selectedItem) {
            holder.linearLayout.setBackgroundResource(R.color.colorMenuHighlight);
        } else {
            holder.linearLayout.setBackgroundResource(R.color.colorTransparent);
        }
    }

    @Override
    public int getItemCount() {
        return listOrder.size();
    }

    /**
     * Reset the color of the row unclicked
     */
    public void resetColor() {
        this.selectedItem = -1;
        notifyItemChanged(this.previousSelected);
        this.previousSelected = -1;

    }

    class HistoryHolder extends RecyclerView.ViewHolder {

        View view;

        TextView statusTextView;

        TextView dateTextView;

        ImageView imageView;

        LinearLayout linearLayout;

        HistoryHolder(View itemView) {
            super(itemView);
            this.view = itemView;
            this.linearLayout = (LinearLayout)itemView.findViewById(R.id.RecyclerRowHistory_LinearLayout);
            this.statusTextView = (TextView) itemView.findViewById(R.id.RecyclerRowHistory_Status_TextView);
            this.dateTextView = (TextView) itemView.findViewById(R.id.RecyclerRowHistory_Date_TextView);
            this.imageView = (ImageView) itemView.findViewById(R.id.RecyclerRowHistory_ImageView);
            this.imageView.setOnClickListener(listener);
            this.statusTextView.setOnClickListener(listener);
            this.dateTextView.setOnClickListener(listener);
        }

        private View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(comm != null) {
                    selectedItem = getAdapterPosition();
                    notifyItemChanged(selectedItem);

                    if(previousSelected != -1) {
                        notifyItemChanged(previousSelected);
                    }
                    previousSelected = selectedItem;

                    switch (v.getId()) {
                        case R.id.RecyclerRowHistory_Status_TextView:
                        case R.id.RecyclerRowHistory_Date_TextView:
                            comm.respondHistoryAdapterInfo(getAdapterPosition());
                            break;
                        case R.id.RecyclerRowHistory_ImageView:
                            comm.respondHistoryAdapterFeedback(getAdapterPosition());
                            break;
                        default:
                            throw new IllegalArgumentException("Unexpected value");
                    }
                }
            }
        };
    }
    public interface Communicator {
        void respondHistoryAdapterFeedback(int position);
        void respondHistoryAdapterInfo(int position);
    }
}
