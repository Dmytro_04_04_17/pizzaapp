package de.h_da.pizzaorder.Adapters.Delivery;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;

/**
 * Created by AEF
 */

public class MyTasksAdapter extends RecyclerView.Adapter<MyTasksAdapter.TaskHolder>{


    //Member variables
    private HashMap<String, ArrayList<OrderDB>> mAllMyTasks;
    private ArrayList<String> mAllMyTasksTimes;

    private Communicator mComm;
    private Context mContext;


    public MyTasksAdapter(HashMap<String,  ArrayList<OrderDB>> allmytasks,Context context){
        this.mAllMyTasks = allmytasks;
        this.mAllMyTasksTimes = new ArrayList<>(mAllMyTasks.keySet());
        this.mContext = context;
        this.mComm = (Communicator) context;
    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View taskView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_row_my_tasks_row, parent, false);
        return new MyTasksAdapter.TaskHolder(taskView, mContext);
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, int position) {
        android.text.format.DateFormat df = new android.text.format.DateFormat();
        Date now = new Date();
        Date orderDate = new Date();
        SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd H:mm:ss");
        try {
            orderDate = dates.parse(mAllMyTasksTimes.get(position));
        } catch (ParseException e) {
            Log.d("xxxx", "error in converting date");
            e.printStackTrace();
        }
        long diff = now.getTime() - orderDate.getTime();
        String text = "The order is submitted at : " + df.format("HH:mm", orderDate);
        text += "\norder contains : "
                + mAllMyTasks.get(mAllMyTasksTimes.get(position)).size()
                + " item"
                + "\n\n"
                + String.valueOf(diff / 60000)
                + " min is passed from the order time";
        if((diff / 60000)>30){
            holder.tvOrderName.setBackgroundResource(R.color.colorOverDueOrder);
        }
        else {
            holder.tvOrderName.setBackgroundResource(R.color.colorNormalOrder);
        }
        holder.tvOrderName.setText(text);
    }

    @Override
    public int getItemCount() {
        return mAllMyTasks.size();
    }

    public class TaskHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvOrderName;
        Context context;

        public TaskHolder(View v, Context c) {
            super(v);
            context = c;
            tvOrderName = (TextView) v.findViewById(R.id.RecyclerRow_My_Tasks_Order_Name);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mComm != null) {
                /*
                * send the selected orderId to Delivery Activity to open task description fragment
                * */
                ArrayList<OrderDB> itemsOfOrder = mAllMyTasks.get(mAllMyTasksTimes.get(getAdapterPosition()));
                mComm.respondOneTaskSelected(itemsOfOrder);
            }
        }
    }

    public interface Communicator {
        void respondOneTaskSelected(ArrayList<OrderDB> selectedTaskItems);
    }

}
