package de.h_da.pizzaorder.Adapters.Customer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.R;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuHolder> {

    private List<MenuDB> listMenu;

    private Communicator comm;

    private int selectedItem = -1;

    private int previousSelected = -1;

    public MenuAdapter(List<MenuDB> list, Context context) {
        this.listMenu = list;
        this.comm = (Communicator) context;
    }

    @Override
    public MenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_row_menu, parent, false);
        return new MenuHolder(view);
    }

    @Override
    public void onBindViewHolder(MenuHolder holder, int position) {
        holder.textView.setText(listMenu.get(position).getName());
        if(position == selectedItem) {
            holder.textView.setBackgroundResource(R.color.colorMenuHighlight);
        } else {
            holder.textView.setBackgroundResource(R.color.colorTransparent);
        }
    }


    @Override
    public int getItemCount() {
        return this.listMenu.size();
    }


    class MenuHolder extends RecyclerView.ViewHolder{

        TextView textView;

        MenuHolder(View itemView) {
            super(itemView);
            this.textView = (TextView)itemView.findViewById(R.id.RecyclerRowMenu_Text_TextView);
            this.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (comm != null) {
                        selectedItem = getAdapterPosition();
                        comm.respondMenuAdapter(getAdapterPosition());
                        notifyItemChanged(selectedItem);

                        if(previousSelected != -1) {
                            notifyItemChanged(previousSelected);
                        }
                        previousSelected = selectedItem;

                    }
                }
            });
        }
    }

    public interface Communicator {
        void respondMenuAdapter( int position);
    }
}
