package de.h_da.pizzaorder.Activities.Delivery;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Activities.Customer.SettingsActivity;
import de.h_da.pizzaorder.Adapters.Delivery.AllOrdersAdapter;
import de.h_da.pizzaorder.Adapters.Delivery.MyTasksAdapter;
import de.h_da.pizzaorder.Fragments.Delivery.DeliveryAllOrdersFragment;
import de.h_da.pizzaorder.Fragments.Delivery.DeliveryMyTaskDetailsFragment;
import de.h_da.pizzaorder.Fragments.Delivery.DeliveryMyTasksListFragment;
import de.h_da.pizzaorder.Fragments.Delivery.DeliveryOrderDetailsFragment;
import de.h_da.pizzaorder.Fragments.Delivery.DeliveryStatisticsFragment;

import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.NetworkHelper;
import de.h_da.pizzaorder.Utilities.OrdersHelper;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Response;


public class DeliveryActivity extends BaseActivity
        implements DeliveryStatisticsFragment.Communicator
        , DeliveryAllOrdersFragment.Communicator
        , AllOrdersAdapter.Communicator
        , DeliveryOrderDetailsFragment.Communicator
        , DeliveryMyTasksListFragment.Communicator
        , MyTasksAdapter.Communicator
        , DeliveryMyTaskDetailsFragment.Communicator {

    /*
    * contains the stats
    * */
    int[] stats;
    OrdersHelper ordersHelper = OrdersHelper.getInstance();
    //Members
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private DeliveryStatisticsFragment deliveryStatisticsFragment;
    private DeliveryAllOrdersFragment deliveryAllOrdersFragment;
    private DeliveryOrderDetailsFragment deliveryOrderDetailsFragment;
    private DeliveryMyTasksListFragment deliveryMyTasksListFragment;
    private DeliveryMyTaskDetailsFragment deliveryMyTaskDetailsFragment;

    private Toolbar toolbar;

    private ServiceDialog dialog;

    private ProgressDialog loading;

    //parameters
    private RetrofitInterface retrofit = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);
    private ServiceSharedPrefs prefs = ServiceSharedPrefs.getInstance();


    /*
    * it contains all the today orders
    * */
    private ArrayList<OrderDB> mTodayOrdersList = new ArrayList<>();
    /*
    * categorized all today orders by customerId
    * Key : customerId
    * Values : List of the orders
    * */
    private HashMap<String, ArrayList<OrderDB>> mAllOrdersCategorizedByDateTime = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);

        toolbar = (Toolbar) findViewById(R.id.DeliveryActivity_Toolbar);
        setSupportActionBar(toolbar);

        dialog = ServiceDialog.getInstance();

        initialiseLoading();

        /*
        * Check Server is reachable
        * */
        showTheLoading();
        if (NetworkHelper.isServerReachable()) {
            stats = new int[4];

            /*
            * refresh the data of Today orders each time that Activity is being created
            * */
            if (!refreshAllTodayListsSyncTask()) {
                listAreNotRefreshedAlert();
            }

            /*
            * Shows Statistics and All Today Ready Orders fragments
            * */
            showFragments();

        } else {
            hideTheLoading();
            notReachableAlert();
        }
    }

    private void initialiseLoading() {
        loading = new ProgressDialog(DeliveryActivity.this);
        loading.setMessage("Please Wait ...");
        loading.setCancelable(false);
        loading.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.delivery_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Menu_Settings:
                this.showSettingsActivity();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Shows SettingsActivity
     */
    private void showSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        this.startActivity(intent);
    }

    /**
     *  Shows Statistics and All Today Ready Orders fragments
     *  it is called after getting all today ready orders.
     *  used in onCreate
     */
    private void showFragments() {

        /*
        * Add delivery statistics fragment to delivery activity
        * */
        stats = getStats(mAllOrdersCategorizedByDateTime);

        this.deliveryStatisticsFragment = DeliveryStatisticsFragment.newInstance(stats[0], stats[1], stats[2], stats[3]);

        /*
        * Add delivery all orders fragment to delivery activity
        * */
        this.deliveryAllOrdersFragment = DeliveryAllOrdersFragment
                .newInstance(ordersHelper.getReadyOrdersWhithoutDeliveryId(mAllOrdersCategorizedByDateTime));

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.DeliveryActivity_StatisticsFrameLayout, deliveryStatisticsFragment, "delivery_statistics");
        transaction.add(R.id.DeliveryActivity_BottomFrameLayout, deliveryAllOrdersFragment, "delivery_all_orders");

        transaction.commit();
    }

    @Override
    public void respondOneOrderSelected(final String selectedOrderTime, final ArrayList<OrderDB> selectedOrderItems) {

        /*
        * Check Server is reachable
         */
        if (NetworkHelper.isServerReachable()) {
            /*
             * Shows a progress dialog
             */
            showTheLoading();

            /*
             * start a new thread to do the heavy task
             */
            Thread backT = new Thread(operationInOneOrderSelected(selectedOrderItems));
            backT.start();
        } else {
            notReachableAlert();
        }

    }

    private Runnable operationInOneOrderSelected(final ArrayList<OrderDB> selectedOrderItems) {
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {

                /*
                * Get customer and menu of this order
                * In here we have only the id of customer then we should query our DB again
                * and also we don't have the menus then we should query DB for the names of the menus
                * */

                final UserDB[] customer = new UserDB[1];
                final MenuDB[][] menus = {new MenuDB[1]};

                /*use the id of customerId from the first item in orders*/
                final int customer_id = selectedOrderItems.get(0).getCustomerId();
                /*
                * create an array containing the menus of the selected order
                 */
                final int[] menu_ids = new int[selectedOrderItems.size()];
                for (int i = 0; i < selectedOrderItems.size(); i++) {
                    menu_ids[i] = selectedOrderItems.get(i).getMenuId();
                }

                /*
                * Call the server for getting customer of the selected order
                * */
                Call<UserDB> callForCustomer = retrofit.getOneUser(customer_id, prefs.getToken());
                Response<UserDB> responseForCustomer = null;
                try {
                    responseForCustomer = callForCustomer.execute();
                    if (responseForCustomer.isSuccessful()) {
                        if (responseForCustomer.body() != null) {
                            customer[0] = responseForCustomer.body();
                        } else {
                            showError("Customer is not legal!");
                        }
                    } else {
                        showError(responseForCustomer.errorBody().string());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    hideTheLoading();
                    showError(e.getMessage());
                }


                /*
                * Used for orders with more than one menu in it
                * Call the server for getting the information of menus in the selected order
                * */
                if (menu_ids.length > 1) {
                    String ids = "";
                    for (int i = 0; i < menu_ids.length; i++) {
                        if (i == menu_ids.length - 1) {
                            ids += String.valueOf(menu_ids[i]);
                        } else {
                            ids += String.valueOf(menu_ids[i]) + ",";
                        }
                    }

                    Call<MenuDB[]> callForMenus = retrofit.getOrderMenus(ids, prefs.getToken());
                    Response<MenuDB[]> responseForMenu = null;
                    try {
                        responseForMenu = callForMenus.execute();
                        if (responseForMenu.isSuccessful()) {

                            if (responseForMenu.body() != null) {
                                menus[0] = responseForMenu.body();
                            } else {
                                showError("Illegal menus");
                            }
                        } else {
                            showError(responseForMenu.errorBody().string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        hideTheLoading();
                        showError(e.getMessage());
                    }

                /*
                * Used for orders with one menu in it
                * Call the server for getting the information of menu in the selected order
                * */
                } else {
                    Call<MenuDB> callForMenu = retrofit.getCurrentMenu(selectedOrderItems.get(0).getMenuId(), prefs.getToken());
                    Response<MenuDB> responseForMenu = null;
                    try {
                        responseForMenu = callForMenu.execute();
                        if (responseForMenu.isSuccessful()) {

                            if (responseForMenu.body() != null) {
                                menus[0][0] = responseForMenu.body();
                            } else {
                                showError("Illegal menu");
                            }
                        } else {
                            showError(responseForMenu.errorBody().string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        hideTheLoading();
                        showError(e.getMessage());
                    }
                }


                /*
                * Hide the progress dialog
                * */
                hideTheLoading();


                /*
                * Create a new DeliveryOrderDetailsFragment and replace it with DeliveryAllOrdersFragment
                * */
                deliveryOrderDetailsFragment = DeliveryOrderDetailsFragment
                        .newInstance(selectedOrderItems, menus[0], customer[0]);


                /*
                * refresh the data of Today ready orders each time an action is being happened
                * */
                if (!refreshAllTodayListsSyncTask()) {
                    listAreNotRefreshedAlert();
                }


                stats = getStats(mAllOrdersCategorizedByDateTime);

                /*
                * Open proper fragments based on the happened Action
                * */
                deliveryStatisticsFragment = DeliveryStatisticsFragment.newInstance(stats[0], stats[1], stats[2], stats[3]);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                deleteAllCurrentFragments(fragmentManager);
                transaction.add(R.id.DeliveryActivity_StatisticsFrameLayout, deliveryStatisticsFragment, "delivery_statistics");
                transaction.add(R.id.DeliveryActivity_BottomFrameLayout, deliveryOrderDetailsFragment, "delivery_order_details");
                transaction.commit();
            }
        };
        return runnable;
    }

    @Override
    public void respondShowMyTaskListButton() {
        /*
        * Check Server is reachable
        */
        if (NetworkHelper.isServerReachable()) {
            /*
            * refresh the data of Today ready orders each time an action is being happened
            * */
            if (!refreshAllTodayListsSyncTask()) {
                listAreNotRefreshedAlert();
            }
            stats = getStats(mAllOrdersCategorizedByDateTime);

            /*
            * Open proper fragments based on the happened Action
            * */
            this.deliveryStatisticsFragment = DeliveryStatisticsFragment.newInstance(stats[0], stats[1], stats[2], stats[3]);
            this.deliveryMyTasksListFragment = DeliveryMyTasksListFragment
                    .newInstance(ordersHelper.getMyTasks(mAllOrdersCategorizedByDateTime));
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            deleteAllCurrentFragments(fragmentManager);
            transaction.add(R.id.DeliveryActivity_StatisticsFrameLayout, deliveryStatisticsFragment, "delivery_statistics");
            transaction.add(R.id.DeliveryActivity_BottomFrameLayout, deliveryMyTasksListFragment, "delivery_my_tasks_list");
            transaction.commit();
        } else {
            notReachableAlert();
        }
    }

    @Override
    public void respondRefreshOrdersListButton() {
        /*
        * Check Server is reachable
        */
        if (NetworkHelper.isServerReachable()) {
            /*
            * refresh the data of Today ready orders each time an action is being happened
            * */
            if (!refreshAllTodayListsSyncTask()) {
                listAreNotRefreshedAlert();
            }
            stats = getStats(mAllOrdersCategorizedByDateTime);
            /*
            * Open proper fragments based on the happened Action
            * */
            this.deliveryStatisticsFragment = DeliveryStatisticsFragment.newInstance(stats[0], stats[1], stats[2], stats[3]);
            this.deliveryAllOrdersFragment = DeliveryAllOrdersFragment
                    .newInstance(ordersHelper.getReadyOrdersWhithoutDeliveryId(mAllOrdersCategorizedByDateTime));
            deleteAllCurrentFragments(fragmentManager);
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.DeliveryActivity_StatisticsFrameLayout, deliveryStatisticsFragment, "delivery_statistics");
            transaction.add(R.id.DeliveryActivity_BottomFrameLayout, deliveryAllOrdersFragment, "delivery_all_orders");
            transaction.commit();
        } else {
            notReachableAlert();
        }
    }

    @Override
    public void respondShowOrdersList() {
        /*
        * Check Server is reachable
        */
        if (NetworkHelper.isServerReachable()) {
            /*
            * refresh the data of Today ready orders each time an action is being happened
            * */
            if (!refreshAllTodayListsSyncTask()) {
                listAreNotRefreshedAlert();
            }
            stats = getStats(mAllOrdersCategorizedByDateTime);

            /*
            * Open proper fragments based on the happened Action
            * */
            this.deliveryStatisticsFragment = DeliveryStatisticsFragment.newInstance(stats[0], stats[1], stats[2], stats[3]);
            this.deliveryAllOrdersFragment = DeliveryAllOrdersFragment
                    .newInstance(ordersHelper.getReadyOrdersWhithoutDeliveryId(mAllOrdersCategorizedByDateTime));
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            deleteAllCurrentFragments(fragmentManager);
            transaction.add(R.id.DeliveryActivity_StatisticsFrameLayout, deliveryStatisticsFragment, "delivery_statistics");
            transaction.add(R.id.DeliveryActivity_BottomFrameLayout, deliveryAllOrdersFragment, "delivery_all_orders");
            transaction.commit();
        } else {
            notReachableAlert();
        }
    }

    @Override
    public void respondIWillDeliverItButton(final int[] ordersId) {
        showTheLoading();
        /*
        * Check Server is reachable
        */
        if (NetworkHelper.isServerReachable()) {
            /*
            * Start a thread to update the fields
            * */
            updateDeliveryIdFieldSyncTask(ordersId);
            /*
            * Shows the Today orders list again after updating the order fields
            * */
            respondShowOrdersList();
        } else {
            hideTheLoading();
            notReachableAlert();
        }
    }

    @Override
    public void respondBackToOrderList() {
        /*
        * Check Server is reachable
        */
        if (NetworkHelper.isServerReachable()) {
            /*
            * refresh the data of Today ready orders each time an action is being happened
            * */
            if (!refreshAllTodayListsSyncTask()) {
                listAreNotRefreshedAlert();
            }
            stats = getStats(mAllOrdersCategorizedByDateTime);
            /*
            * Open proper fragments based on the happened Action
            * */
            this.deliveryStatisticsFragment = DeliveryStatisticsFragment.newInstance(stats[0], stats[1], stats[2], stats[3]);
            this.deliveryAllOrdersFragment = DeliveryAllOrdersFragment
                    .newInstance(ordersHelper.getReadyOrdersWhithoutDeliveryId(mAllOrdersCategorizedByDateTime));
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            deleteAllCurrentFragments(fragmentManager);
            transaction.add(R.id.DeliveryActivity_StatisticsFrameLayout, deliveryStatisticsFragment, "delivery_statistics");
            transaction.add(R.id.DeliveryActivity_BottomFrameLayout, deliveryAllOrdersFragment, "delivery_all_orders");
            transaction.commit();
        } else {
            notReachableAlert();
        }
    }

    @Override
    public void respondOneTaskSelected(final ArrayList<OrderDB> selectedTaskItems) {
        /*
        * Check Server is reachable
        */
        if (NetworkHelper.isServerReachable()) {
            showTheLoading();
            Thread backT = new Thread(operationInOneTaskSelected(selectedTaskItems));
            backT.start();
            while (backT.isAlive()) {
                //do nothing
            }
        } else {
            notReachableAlert();
        }
    }

    private Runnable operationInOneTaskSelected(final ArrayList<OrderDB> selectedTaskItems) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                /*
                * Get customer and menu of this order
                * In here we have only the id of customer then we should query our DB again
                * and also we don't have the menus then we should query DB for the names of the menus
                * */

                final UserDB[] customer = new UserDB[1];
                final MenuDB[][] menu = {new MenuDB[1]};

                final int customer_id = selectedTaskItems.get(0).getCustomerId();
                final int[] menu_ids = new int[selectedTaskItems.size()];
                for (int i = 0; i < selectedTaskItems.size(); i++) {
                    menu_ids[i] = selectedTaskItems.get(i).getMenuId();
                }
                /*
                * Call the server for getting customer of the selected task
                * */
                Call<UserDB> callForCustomer = retrofit.getOneUser(customer_id, prefs.getToken());
                Response<UserDB> responseForCustomer = null;
                try {
                    responseForCustomer = callForCustomer.execute();
                    if (responseForCustomer.isSuccessful()) {
                        if (responseForCustomer.body() != null) {
                            customer[0] = responseForCustomer.body();
                        } else {
                            showError(responseForCustomer.errorBody().string());
                        }
                        hideTheLoading();
                    } else {
                        hideTheLoading();
                        showError("Illegal customer");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    hideTheLoading();
                    showError(e.getMessage());
                }


                /*
                * Used for orders with more than one menu in it
                * Call the server for getting the information of menus in the selected task
                * */
                if (menu_ids.length > 1) {
                    String ids = "";
                    for (int i = 0; i < menu_ids.length; i++) {
                        if (i == menu_ids.length - 1) {
                            ids += String.valueOf(menu_ids[i]);
                        } else {
                            ids += String.valueOf(menu_ids[i]) + ",";
                        }
                    }

                    Call<MenuDB[]> callForMenus = retrofit.getOrderMenus(ids, prefs.getToken());
                    Response<MenuDB[]> responseForMenu = null;
                    try {
                        responseForMenu = callForMenus.execute();
                        if (responseForMenu.isSuccessful()) {
                            if (responseForMenu.body() != null) {
                                menu[0] = responseForMenu.body();
                            } else {
                                showError("Illegal menus");
                            }
                            hideTheLoading();
                        } else {
                            hideTheLoading();
                            showError(responseForMenu.errorBody().string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        hideTheLoading();
                        showError(e.getMessage());
                    }

                }

                /*
                * Used for orders with one menu in it
                * Call the server for getting the information of menu in the selected task
                * */
                else {
                    Call<MenuDB> callForMenu = retrofit.getCurrentMenu(selectedTaskItems.get(0).getMenuId(), prefs.getToken());
                    Response<MenuDB> responseForMenu = null;
                    try {
                        responseForMenu = callForMenu.execute();
                        if (responseForMenu.isSuccessful()) {
                            if (responseForMenu.body() != null) {
                                menu[0][0] = responseForMenu.body();
                            } else {
                                showError("Illegal menu");
                            }
                            hideTheLoading();
                        } else {
                            hideTheLoading();
                            showError(responseForMenu.errorBody().string());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        hideTheLoading();
                        showError(e.getMessage());
                    }
                }

                /*
                * Hide the progress dialog
                * */
                hideTheLoading();

                /*
                * refresh the data of Today ready orders each time an action is being happened
                * */
                if (!refreshAllTodayListsSyncTask()) {
                    listAreNotRefreshedAlert();
                }
                stats = getStats(mAllOrdersCategorizedByDateTime);


                /*
                * Create a new DeliveryTaskDetailsFragment and repalce it with DeliveryAllOrdersFragment
                * */
                deliveryMyTaskDetailsFragment = DeliveryMyTaskDetailsFragment
                        .newInstance(selectedTaskItems, menu[0], customer[0]);

                deliveryStatisticsFragment = DeliveryStatisticsFragment.newInstance(stats[0], stats[1], stats[2], stats[3]);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                deleteAllCurrentFragments(fragmentManager);
                transaction.add(R.id.DeliveryActivity_StatisticsFrameLayout, deliveryStatisticsFragment, "delivery_statistics");
                transaction.add(R.id.DeliveryActivity_BottomFrameLayout, deliveryMyTaskDetailsFragment, "delivery_task_details");
                transaction.commit();
            }
        };
        return runnable;
    }

    @Override
    public void respondDeliveredButton(int[] ordersId) {

        showTheLoading();
        /*
        * Check Server is reachable
        */
        if (NetworkHelper.isServerReachable()) {

            /*
            * Start a thread to update the fields
            * */
            updateStatusFieldSyncTask(ordersId);
            /*
            * Shows the tasks list again after updating the order fields
            * */
            respondShowMyTaskListButton();
        } else {
            hideTheLoading();
            notReachableAlert();
        }

    }

    @Override
    public void respondBackToMyTasksButton() {
        respondShowMyTaskListButton();
    }

    private boolean refreshAllTodayListsSyncTask() {

        showTheLoading();

        final boolean[] result = {false};
        Thread t1 = null;
        t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                /*
                * Get today date time for calling the Server
                * */
                String filter = "time,cs," + getDateTime();

                Call<Map<String, List<OrderDB>>> call = retrofit.getUserOrders(prefs.getToken(), filter);
                Response<Map<String, List<OrderDB>>> response = null;

                try {
                    response = call.execute();
                    if (response.isSuccessful()) {
                        mTodayOrdersList.clear();
                        mTodayOrdersList = (ArrayList<OrderDB>) response.body().get("orders");
                        mAllOrdersCategorizedByDateTime = ordersHelper.getCategorizedOrdersByDateTime(mTodayOrdersList);
                        result[0] = true;
                    } else {
                        showError(response.errorBody().string());
                    }
                    hideTheLoading();
                } catch (IOException e) {
                    e.printStackTrace();
                    showError(e.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                    showError(e.getMessage());
                }
            }
        });
        t1.start();
        while (t1.isAlive()) {
            //do nothing
        }
        return result[0];
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private int[] getStats(HashMap<String, ArrayList<OrderDB>> allordersCategorizedByDateTime) {
        int[] stats = new int[4];
        // stats [0] : all today orders
        stats[0] = allordersCategorizedByDateTime.size();

        // stats[1] : ready orders
        stats[1] = ordersHelper.getReadyOrdersWhithoutDeliveryId(allordersCategorizedByDateTime).size();

        // stats[2] : myTasks
        HashMap<String, ArrayList<OrderDB>> myTasks = ordersHelper.getMyTasks(allordersCategorizedByDateTime);
        stats[2] = myTasks.size();

        //stats[3] : myTasksDone
        stats[3] = ordersHelper.getMyTasksDelivered(myTasks).size();

        return stats;
    }

    private void updateDeliveryIdFieldSyncTask(final int[] ordersId) {
        showTheLoading();
        final String[] message = {""};
        Thread t1 = null;
        t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ordersId.length; i++) {
                    Call<Integer> call = retrofit.setDeliveryId(ordersId[i], prefs.getToken(), prefs.getUserId());
                    Response<Integer> response = null;
                    try {
                        response = call.execute();
                        if (response.isSuccessful()) {
                            if (response != null) {
                                if (response.body() > 0) {
                                    message[0] = "The order is added to your tasks";
                                } else {
                                    //TODO:  make it better
                                    message[0] += "Nothing happen. check tasks and orders";
                                }
                            }
                            hideTheLoading();
                        } else {
                            //TODO:  make it better
                            message[0] += response.errorBody().toString();
                            hideTheLoading();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        showError(e.getMessage());
                        hideTheLoading();
                    }
                }
            }
        });
        t1.start();
        while (t1.isAlive()) {
            //do nothing
        }
        dialog.createDialog(DeliveryActivity.this, message[0]).show();
    }

    private void updateStatusFieldSyncTask(final int[] ordersId) {
        showTheLoading();
        final String[] message = {""};
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ordersId.length; i++) {
                    Call<Integer> call = retrofit.setStatus(ordersId[i], prefs.getToken(), "delivered");
                    Response<Integer> response = null;
                    try {
                        response = call.execute();
                        if (response.isSuccessful()) {
                            if (response != null) {
                                if (response.body() > 0) {
                                    message[0] = "Order delivered";
                                    hideTheLoading();
                                } else {
                                    hideTheLoading();
                                    message[0] += "Nothing changed.Something goes wrong!";
                                }
                            }
                        } else {
                            hideTheLoading();
                            message[0] += response.errorBody().toString();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        showError(e.getMessage());
                        hideTheLoading();
                    }
                }
            }
        });
        t1.start();
        while (t1.isAlive()) {
            //do nothing
        }
        dialog.createDialog(DeliveryActivity.this, message[0]).show();
    }

    private void deleteAllCurrentFragments(FragmentManager fragmentManager) {
        for (Fragment frag : fragmentManager.getFragments()) {
            if (frag != null)
                fragmentManager.beginTransaction().remove(frag).commit();
        }
    }

    private void notReachableAlert() {
        dialog.createDialog(DeliveryActivity.this, getString(R.string.not_reachable));
    }

    private void showError(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.createDialog(DeliveryActivity.this, message).show();
            }
        });
    }

    private void listAreNotRefreshedAlert() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.createDialog(DeliveryActivity.this, "Lists are not refreshed!").show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        this.backPressedDialog();
    }

    /**
     * Shows an alert dialog(when back button pressed)
     */
    private void backPressedDialog() {
        ServiceDialog.getInstance().createDialog(this, null, getString(R.string.dialog_logout_title), null,
                getString(R.string.Generall_Ok_Text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, getString(R.string.Generall_Cancel_Text), null, false, true);
    }

    private void hideTheLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (loading.isShowing())
                    loading.dismiss();
            }
        });
    }

    private void showTheLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!loading.isShowing())
                    loading.show();
            }
        });
    }
}
    