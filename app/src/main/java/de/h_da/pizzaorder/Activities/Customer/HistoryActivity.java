package de.h_da.pizzaorder.Activities.Customer;

import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Adapters.Customer.HistoryAdapter;
import de.h_da.pizzaorder.Fragments.Customer.CustomerFeedbackFragment;
import de.h_da.pizzaorder.Fragments.Customer.CustomerFragmentDescription;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener,
        HistoryAdapter.Communicator, CustomerFragmentDescription.Communicator, CustomerFeedbackFragment.Communicator {

    private RecyclerView recyclerView;

    private HistoryAdapter historyAdapter;

    private SwipeRefreshLayout swipeRefreshLayout;

    private LinearLayout linearLayout;

    private Toolbar toolbar;

    private AlertDialog dialog;

    // orders from a db for the current user
    private List<OrderDB> listOrder = new ArrayList<>();

    private CustomerFragmentDescription customerFragmentDescription;

    private CustomerFeedbackFragment customerFeedbackFragment;

    private RetrofitInterface retrofit = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);

    private ServiceSharedPrefs prefs = ServiceSharedPrefs.getInstance();

    private ServiceDialog serviceDialog = ServiceDialog.getInstance();

    private FragmentManager fragmentManager = getSupportFragmentManager();

    private boolean needRefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_history);
        this.initializeUI();
        this.setListeners();
        this.setData();
        this.dialog = ServiceDialog.getInstance().createLoader(this);
        this.dialog.show();
        this.loadData();
    }

    /**
     * Initalizes xml widgets
     */
    private void initializeUI() {
        this.toolbar = (Toolbar) this.findViewById(R.id.HistoryActivity_Toolbar) ;
        this.recyclerView = (RecyclerView) this.findViewById(R.id.HistoryActivity_RecyclerView);
        this.linearLayout = (LinearLayout) this.findViewById(R.id.HistoryActivity_LinearLayout);
        this.swipeRefreshLayout = (SwipeRefreshLayout) this.findViewById(R.id.HistoryActivity_SwipeRefreshLayout);
        this.swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN);
    }

    /**
     * Sets listeners to widgets
     */
    private void setListeners() {
        this.swipeRefreshLayout.setOnRefreshListener(this);
    }

    /**
     * Sets data to widgets
     */
    private void setData() {
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.historyAdapter = new HistoryAdapter(this, this.listOrder);
        this.recyclerView.setAdapter(this.historyAdapter);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    /**
     * Make request for the info of the current user
     */
    private void loadData() {
        Call<Map<String, List<OrderDB>>> call = this.retrofit.getUserOrders(this.prefs.getToken(),
                "customerId,eq," + this.prefs.getUserId());
        call.enqueue(new Callback<Map<String, List<OrderDB>>>() {
            @Override
            public void onResponse(Call<Map<String, List<OrderDB>>> call, Response<Map<String, List<OrderDB>>> response) {
                dialog.dismiss();
                updateData(response.body().get("orders"));
            }

            @Override
            public void onFailure(Call<Map<String, List<OrderDB>>> call, Throwable t) {
                dialog.dismiss();
                stopRefreshing();
            }
        });
    }

    /**
     * Updates data
     * @param list - received data after the request
     */
    private void updateData(List<OrderDB> list) {
        if(this.listOrder.size() != 0) {
            this.listOrder.clear();
        }

        this.listOrder.addAll(list);
        this.historyAdapter.notifyDataSetChanged();
        this.stopRefreshing();
        if(this.needRefresh) {
            this.linearLayout.setVisibility(View.VISIBLE);
            this.needRefresh = false;
        }
    }

    /**
     * Stop refreshing of a refresh layout
     */
    private void stopRefreshing() {
        if(this.swipeRefreshLayout.isRefreshing()) {
            this.swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        this.swipeRefreshLayout.setRefreshing(true);
        this.loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Make request for receiving the info about the menu clicked
     * @param id - clicked menu
     */
    private void requestForCurrentMenu(int id) {
        Call<MenuDB> call = this.retrofit.getCurrentMenu(id, this.prefs.getToken());
        call.enqueue(new Callback<MenuDB>() {
            @Override
            public void onResponse(Call<MenuDB> call, Response<MenuDB> response) {
                customerFragmentDescription = CustomerFragmentDescription.newInstance(response.body(), true);

                if(portraitMode) {
                    linearLayout.setVisibility(View.INVISIBLE);
                    fragmentManager.beginTransaction().
                            add(R.id.HistoryActivity_FrameLayout, customerFragmentDescription).commit();
                    swipeRefreshLayout.setEnabled(false);
                } else {
                    fragmentManager.beginTransaction().
                            replace(R.id.HistoryActivity_FrameLayout_Second, customerFragmentDescription).commit();
                }
            }

            @Override
            public void onFailure(Call<MenuDB> call, Throwable t) {
                serviceDialog.createDialog(HistoryActivity.this, t.getMessage());
            }
        });
    }

    /**
     * Handles
     * @param position - position in adapter clicked
     */
    @Override
    public void respondHistoryAdapterFeedback(int position) {
        if (this.listOrder.get(position).getStatus().equals("delivered") &&
                this.listOrder.get(position).getCustomerFeedback().equals("")) {
            this.customerFeedbackFragment = CustomerFeedbackFragment.newInstance(this.listOrder.get(position), false);
           this.placeRegardingOrientation();
        } else if (this.listOrder.get(position).getStatus().equals("delivered") &&
                this.listOrder.get(position).getCustomerFeedback().length() > 0) {
            this.customerFeedbackFragment = CustomerFeedbackFragment.newInstance(this.listOrder.get(position), true);
           this.placeRegardingOrientation();
        }
    }

    /**
     * Checks which orientation right now used and places fragments regarding
     */
    private void placeRegardingOrientation() {
        if(this.portraitMode) {
            this.linearLayout.setVisibility(View.INVISIBLE);
            this.fragmentManager.beginTransaction().replace(R.id.HistoryActivity_FrameLayout, this.customerFeedbackFragment).commit();
            this.swipeRefreshLayout.setEnabled(false);
        } else {
            this.fragmentManager.beginTransaction().replace(R.id.HistoryActivity_FrameLayout_Second, this.customerFeedbackFragment).commit();
        }
    }

    /**
     * Handles menu clicked
     * @param position - position in a adapter
     */
    @Override
    public void respondHistoryAdapterInfo(int position) {
        //TODO Show menu ordered
        this.requestForCurrentMenu(this.listOrder.get(position).getMenuId());
    }

    /**
     * Handles back button clicked
     */
    @Override
    public void respondCustomerFragmentDescriptionBack() {
        if(this.portraitMode) {
            fragmentManager.beginTransaction().remove(customerFragmentDescription).commit();
            this.historyAdapter.resetColor();
            this.linearLayout.setVisibility(View.VISIBLE);
            this.swipeRefreshLayout.setEnabled(true);

        }
    }

    @Override
    public void respondCustomerFragmentDescriptionAdd(MenuDB menu) {

    }

    @Override
    public void respondCustomerFragmentDescriptionOrder(MenuDB menu) {

    }

    /**
     * Handles send button for feedback
     */
    @Override
    public void respondCustomerFeedbackSendButton() {
        fragmentManager.beginTransaction().remove(customerFeedbackFragment).commit();
        this.historyAdapter.resetColor();
        this.onRefresh();
        this.linearLayout.setVisibility(View.VISIBLE);
    }

    /**
     * Handles back button for feedback
     */
    @Override
    public void respondCustomerFeedbackBackButton() {
        fragmentManager.beginTransaction().remove(customerFeedbackFragment).commit();
        this.historyAdapter.resetColor();
        this.linearLayout.setVisibility(View.VISIBLE);
    }
}
