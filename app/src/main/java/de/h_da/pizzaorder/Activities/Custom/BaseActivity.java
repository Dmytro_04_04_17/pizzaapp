package de.h_da.pizzaorder.Activities.Custom;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import de.h_da.pizzaorder.R;

/**
 * Determines the size of the device and fixes orientation
 */
public class BaseActivity extends AppCompatActivity {

    protected boolean portraitMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getBoolean(R.bool.isTablet)) {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            this.portraitMode = true;
        }
    }

    public boolean getPortraitMode() {
        return this.portraitMode;
    }

}
