package de.h_da.pizzaorder.Activities.Custom;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import de.h_da.pizzaorder.Activities.Admin.AdminActivity;
import de.h_da.pizzaorder.Activities.Chef.ChefActivity;
import de.h_da.pizzaorder.Activities.Customer.CustomerActivity;
import de.h_da.pizzaorder.Activities.Delivery.DeliveryActivity;
import de.h_da.pizzaorder.Models.ApiDB;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.NetworkHelper;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private Button loginButton;

    private EditText loginEditText;

    private EditText passwordEditText;

    private TextView createNewUserTextView;

    private ServiceDialog dialog = ServiceDialog.getInstance();

    private ServiceSharedPrefs sharedPrefs = ServiceSharedPrefs.getInstance();

    private RetrofitInterface retrofitInterfaceObject = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View view = LayoutInflater.from(this).inflate(R.layout.activity_login, null);
        super.onCreate(savedInstanceState);
        this.setContentView(view);
        if (this.sharedPrefs.checkFirstLaunch(this)) {
            this.dialog.createDialog(this, getString(R.string.first_launch));
            this.sharedPrefs.setFirstLaucnch();
        }
        this.initializeUI(view);
        this.setListeners();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ServiceSharedPrefs.getInstance().dataSaved()) {
            this.loginEditText.setText(sharedPrefs.getLogin());
            this.passwordEditText.setText(sharedPrefs.getPassword());
        }
    }

    /**
     * initializes widgets from xml
     * @param view - current view
     */
    private void initializeUI(View view) {
        this.loginButton = (Button) view.findViewById(R.id.LoginActivity_Login_Button);
        this.loginEditText = (EditText) view.findViewById(R.id.LoginActivity_Login_EditText);
        this.passwordEditText = (EditText) view.findViewById(R.id.LoginActivity_Password_EditText);
        this.createNewUserTextView = (TextView) view.findViewById(R.id.LoginActivity_CreateNew_TextView);
    }

    /**
     * Sets listeners to widgets
     */
    private void setListeners() {
        this.loginButton.setOnClickListener(this);
        this.createNewUserTextView.setOnClickListener(this);
        this.passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    loginButton.performClick();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.LoginActivity_Login_Button:
                String username = String.valueOf(this.loginEditText.getText());
                String password = String.valueOf(this.passwordEditText.getText());
                if (username.length() != 0 && password.length() != 0) {
                    if (NetworkHelper.isServerReachable()) {
                        this.dialog.createLoader(this).show();
                        this.loginProcedure(username, password);
                    } else {
                        this.dialog.createDialog(LoginActivity.this, getString(R.string.not_reachable));
                    }
                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.fill_the_fields), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.LoginActivity_CreateNew_TextView:
                this.startActivity(new Intent(this, NewUserActivity.class));
                break;
            default:
                throw new IllegalArgumentException("Unexpected value");
        }
    }

    /**
     * Makes login procedure
     * @param username - the name entered
     * @param password - the password entered
     */
    private void loginProcedure(final String username, final String password) {

        Call<ApiDB> call = retrofitInterfaceObject.checkCredentials(new UserDB(username, password));
        call.enqueue(new Callback<ApiDB>() {
            @Override
            public void onResponse(Call<ApiDB> call, Response<ApiDB> response) {
                dialog.hideDialog();
                //Unauthorized handling
                if (response.code() == 401) {
                    dialog.createDialog(LoginActivity.this, String.valueOf(response.code())
                            + LoginActivity.this.getString(R.string.login_unauthorized_error_title));
                }
                //Handling the 200 response : 2 possibility
                else if (response.code() == 200) {
                    //if login ok
                    if (response.body().getLogin() != null) {
                        if (response.body().getLogin().contains("yes")) {
                            sharedPrefs.setToken(response.headers().get("X-XSRF-TOKEN"));
                            sharedPrefs.setUserId(Integer.valueOf(response.body().getId()));
                            sharedPrefs.saveData(username, password);
                            /*
                            * Open proper activity based on the user type
                            * */
                            String usertype = response.body().getType();
                            switch (usertype) {
                                case "customer":
                                    LoginActivity.this.startActivity(new Intent(LoginActivity.this, CustomerActivity.class));
                                    break;
                                case "delivery":
                                    LoginActivity.this.startActivity(new Intent(LoginActivity.this, DeliveryActivity.class));
                                    break;
                                case "admin":
                                    LoginActivity.this.startActivity(new Intent(LoginActivity.this, AdminActivity.class));
                                    break;
                                case "chef":
                                    LoginActivity.this.startActivity(new Intent(LoginActivity.this, ChefActivity.class));
                                    break;
                                //TODO: should complete over time
                                default:
                                    dialog.createDialog(LoginActivity.this, "User type does not have any proper activity");
                                    break;
                            }
                        }

                    }
                    //if login not ok
                    else if (response.body().getError().contains("Error in authentication!")) {
                        dialog.createDialog(LoginActivity.this, null, LoginActivity.this.getString(R.string.login_credentials_error_title),
                                LoginActivity.this.getString(R.string.login_credentials_error_message),
                                LoginActivity.this.getString(R.string.Generall_Ok_Text), null, null, null, false, false);
                    }
                }
                //Handle other situations
                else {
                    dialog.createDialog(LoginActivity.this, null, response.code() + " : " +
                                    LoginActivity.this.getString(R.string.login_null_error_title),
                            LoginActivity.this.getString(R.string.login_null_error_message),
                            LoginActivity.this.getString(R.string.Generall_Ok_Text), null, null, null, false, false);
                }
            }

            @Override
            public void onFailure(Call<ApiDB> call, Throwable t) {
                dialog.hideDialog();
                dialog.createDialog(LoginActivity.this, null, LoginActivity.this.getString(R.string.login_null_error_title),
                        t.getMessage() + "\n" + t.getLocalizedMessage(), LoginActivity.this.getString(R.string.Generall_Ok_Text),
                        null, null, null, true, true);
            }
        });
    }
}