package de.h_da.pizzaorder.Activities.Custom;

import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import de.h_da.pizzaorder.Models.ApiDB;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.CreateNewUserHelper;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewUserActivity extends BaseActivity implements View.OnClickListener, Validator.ValidationListener {

    private Button createButton;

    @NotEmpty(trim = true)
    @Length(min = 4, max = 40, message = "Username should be between 4 to 40 characters")
    private EditText usernameEditText;

    @NotEmpty(trim = true)
    @Password
    @Length(max = 45, message = "Maximun lenght is 45 characters")
    private EditText passwordEditText;

    @NotEmpty(trim = true)
    @ConfirmPassword
    @Length(max = 45, message = "Maximun lenght is 45 characters")
    private EditText repasswordEditText;

    @NotEmpty(trim = true)
    @Length(min = 2, max = 40, message = "Name should be between 2 to 40 characters")
    private EditText nameEditText;

    @NotEmpty(trim = true)
    @Length(min = 1, max = 40, message = "Family should be between 1 to 40 characters")
    private EditText familyEditText;

    @NotEmpty(trim = true)
    @Length(min = 1, max = 500, message = "Maximun lenght is 500 characters")
    private EditText addressEditText;

    @NotEmpty(trim = true)
    @Length(max = 40, message = "Maximun lenght is 40 characters")
    @Email
    private EditText emailEditText;

    @NotEmpty(trim = true)
    @Length(min = 1, max = 13, message = "Telephone should be between 1 to 13 digit")
    private EditText telephoneEditText;

    private ServiceSharedPrefs sharedPrefs = ServiceSharedPrefs.getInstance();

    private ServiceDialog dialog = ServiceDialog.getInstance();

    private RetrofitInterface retrofitInterfaceObject = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);

    private Validator validator;

    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View view = LayoutInflater.from(this).inflate(R.layout.activity_new_user, null);
        super.onCreate(savedInstanceState);

        this.setContentView(view);
        this.initializeUI(view);
        this.setListeners();

        toolbar = (Toolbar) findViewById(R.id.NewUserActivity_Toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*
        *  Make back arrow white
        * */
        final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_menu_back);
        upArrow.setColorFilter(ContextCompat.getColor(this,R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Initialize all widgets
     * @param view - current view
     */
    private void initializeUI(View view) {
        this.createButton = (Button) view.findViewById(R.id.NewUserActivity_Create_Button);
        this.usernameEditText = (EditText) view.findViewById(R.id.NewUserActivity_Username_EditText);
        this.passwordEditText = (EditText) view.findViewById(R.id.NewUserActivity_Password_EditText);
        this.repasswordEditText = (EditText) view.findViewById(R.id.NewUserActivity_RePassword_EditText);
        this.nameEditText = (EditText) view.findViewById(R.id.NewUserActivity_Name_EditText);
        this.familyEditText = (EditText) view.findViewById(R.id.NewUserActivity_Family_EditText);
        this.addressEditText = (EditText) view.findViewById(R.id.NewUserActivity_Address_EditText);
        this.emailEditText = (EditText) view.findViewById(R.id.NewUserActivity_Email_EditText);
        this.telephoneEditText = (EditText) view.findViewById(R.id.NewUserActivity_Telephone_EditText);
        validator = new Validator(this);
    }

    /**
     *
     */
    private void setListeners() {
        this.createButton.setOnClickListener(this);
        validator.setValidationListener(this);

        this.addressEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validator.validate();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.NewUserActivity_Create_Button:
                createButton.setEnabled(false);
                createButton.setText(R.string.button_text_create_disabled);
                validator.validate();
                break;
            default:
                throw new IllegalArgumentException("Unexpected value");
        }
    }

    private void createuser(String username
            , String password
            , String name
            , String family
            , String address
            , String email
            , String telephone) {


        //Create a temp user based on the user input
        UserDB newuser = new UserDB(username, password, "customer", name, family, address, email, telephone, 0);


        //Creation user : Phase 1 , phase 2 is inside the phase 1 when the phase 1 is successful
        getTokenForCreating(newuser);


    }

    private void getTokenForCreating(final UserDB newuser) {


        //First get token for creating the new user
        UserDB user = CreateNewUserHelper.getCreateUser();

        //send data to the api and get response
        Call<ApiDB> call = retrofitInterfaceObject.checkCredentials(user);
        call.enqueue(new Callback<ApiDB>() {
            @Override
            public void onResponse(Call<ApiDB> call, Response<ApiDB> response) {
                //Unauthorized handling
                if (response.code() == 401) {
                    dialog.createDialog(NewUserActivity.this, "Error in getting token").show();
                    createButton.setText(R.string.NewUserActivity_Create_Button);
                    createButton.setEnabled(true);
                }
                //Handling the 200 response : 2 possibility
                else if (response.code() == 200) {
                    //if login ok
                    if (response.body().getLogin() != null) {
                        if (response.body().getLogin().contains("yes")) {

                            //creation user : Phase 2
                            createUserByToken(response.headers().get("X-XSRF-TOKEN"), newuser);
                        }
                    }
                    //if login not ok
                    else if (response.body().getError().contains("Error in authentication!")) {
                        dialog.createDialog(NewUserActivity.this, "Error in getting token : you don' have permission").show();
                        createButton.setText(R.string.NewUserActivity_Create_Button);
                        createButton.setEnabled(true);
                    }
                }
                //Handle other situations
                else {
                    dialog.createDialog(NewUserActivity.this, "Something goes wrong").show();
                    createButton.setText(R.string.NewUserActivity_Create_Button);
                    createButton.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<ApiDB> call, Throwable t) {
                dialog.createDialog(NewUserActivity.this, null, NewUserActivity.this.getString(R.string.login_null_error_title),
                        t.getMessage() + "\n" + t.getLocalizedMessage(), NewUserActivity.this.getString(R.string.Generall_Ok_Text),
                        null, null, null, true, true);
                createButton.setText(R.string.NewUserActivity_Create_Button);
                createButton.setEnabled(true);
            }
        });


    }

    private void createUserByToken(String token, UserDB newuser) {
        if (token != null) {

            Call<Integer> call = retrofitInterfaceObject.createUser(token, newuser);

            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    //Unauthorized handling
                    if (response.code() == 401) {
                        dialog.createDialog(NewUserActivity.this, null,
                                String.valueOf(response.code())
                                        + NewUserActivity.this.getString(R.string.login_unauthorized_error_title)
                                , NewUserActivity.this.getString(R.string.login_unauthorized_error_message)
                                , NewUserActivity.this.getString(R.string.Generall_Ok_Text), null, null, null, false, false);
                        createButton.setEnabled(true);
                    }
                    //Handling the 200 response : 2 possibility
                    else if (response.code() == 200) {
                        //if creation ok
                        if (response.body() != null) {
                            if (response.body() > 1) {
                                dialog.createDialog(NewUserActivity.this, "User is created.").show();

                            }
                        }
                        //if creation not ok
                        else {
                            dialog.createDialog(NewUserActivity.this, "Error : User is not created. It already exists").show();
                            createButton.setEnabled(true);
                        }
                    }
                    //Handle other situations
                    else {
                        dialog.createDialog(NewUserActivity.this, "Error : something wrong happened").show();
                        createButton.setEnabled(true);
                    }
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    dialog.createDialog(NewUserActivity.this, "Error :\n" + t.getMessage()).show();
                    createButton.setEnabled(true);
                }
            });
        }
    }

    @Override
    public void onValidationSucceeded() {
        String username = String.valueOf(this.usernameEditText.getText());
        String password = String.valueOf(this.passwordEditText.getText());
        String name = String.valueOf(this.nameEditText.getText());
        String family = String.valueOf(this.familyEditText.getText());
        String address = String.valueOf(this.addressEditText.getText());
        String email = String.valueOf(this.emailEditText.getText());
        String telephone = String.valueOf(this.telephoneEditText.getText());

        createuser(username
                , password
                , name
                , family
                , address
                , email
                , telephone);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError oneError : errors) {
            View errorView = oneError.getView();
            String message = oneError.getCollatedErrorMessage(this);
            if (errorView instanceof EditText) {
                ((EditText) errorView).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
        errors.get(0).getView().setFocusable(true);
        errors.get(0).getView().requestFocus();
        createButton.setText(R.string.NewUserActivity_Create_Button);
        createButton.setEnabled(true);
    }
}
