package de.h_da.pizzaorder.Activities.Chef;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Adapters.Chef.ChefAllAdapter;
import de.h_da.pizzaorder.Adapters.Chef.ChefPagerAdapter;
import de.h_da.pizzaorder.Fragments.Chef.ChefAllOrdersFragment;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChefActivity extends BaseActivity implements
        ChefAllOrdersFragment.Communicator, ChefAllAdapter.Communicator {

    private static final String ALL_ORDERS_INPROGRESS_FILTER = "status,eq,in progress";

    private static final String CHEF_FILTER = "chefId,eq,";

    private static final String ORDERS_ISCOOKING_FILTER = "status,eq,cooking";

    private final String STATUS_COOKING = "cooking";

    private final String STATUS_COOKED = "cooked";

    private final String STATUS_IN_PROGRESS = "in progress";

    // Adapter for displaying fragments
    private ChefPagerAdapter chefPagerAdapter;

    // ViewPager for handling displayed fragments
    private ViewPager viewPager;

    // instance for network requests
    private final RetrofitInterface retrofit = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);

    // Instance for shared preferences
    private final ServiceSharedPrefs prefs = ServiceSharedPrefs.getInstance();

    // Instance for dialog helper class
    private final ServiceDialog dialog = ServiceDialog.getInstance();

    // All orders with a status in progress
    private List<OrderDB> ordersList = new ArrayList<>();

    // All orders of the current Chef with a status in progress
    private List<OrderDB> ownChefsList = new ArrayList<>();

    // All Menus available
    private List<MenuDB> menuList = new ArrayList<>();

    // All Menus which the current Chef is cooking
    private List<MenuDB> ownMenuList = new ArrayList<>();

    // All the ID's of the orders, which a current Chef marked (press "Start Cooking")
    private List<OrderDB> chosenOrders = new ArrayList<>();

    // All the ID's of the orders of a current Chef which are cooked (press "Finish Cooking")
    private List<OrderDB> finishedOrders = new ArrayList<>();

    private List<OrderDB> orderForChange = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chef);
        this.initializeUI();
        this.setData();
        this.requestData(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chef, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            this.requestData(true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.dialog.createDialog(this, null, getString(R.string.dialog_logout_title), null,
                getString(R.string.Generall_Ok_Text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, getString(R.string.Generall_Cancel_Text), null, false, true);
    }

    /**
     * Initialize data from xml
     */
    private void initializeUI() {
        this.viewPager = (ViewPager) findViewById(R.id.ChefActivity_ViewPager);
    }

    /**
     * Set data
     */
    private void setData() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        this.chefPagerAdapter = new ChefPagerAdapter(this, this.getSupportFragmentManager(), this.menuList, this.ownMenuList);
        this.viewPager.setAdapter(this.chefPagerAdapter);
    }

    /**
    * Receive all orders with a status "in progress"
    */
    private void requestData(boolean showLoading) {
        if(showLoading) {
            this.dialog.createLoader(this).show();
        }
        Call<Map<String, List<OrderDB>>> call = this.retrofit.getUserOrders(this.prefs.getToken(), ALL_ORDERS_INPROGRESS_FILTER);
        call.enqueue(new Callback<Map<String, List<OrderDB>>>() {
            @Override
            public void onResponse(Call<Map<String, List<OrderDB>>> call, Response<Map<String, List<OrderDB>>> response) {
                updateDataOrders(response.body().get("orders"), 1);
                requestSpecificData();
            }

            @Override
            public void onFailure(Call<Map<String, List<OrderDB>>> call, Throwable t) {
                stopRefreshing();
                dialog.createDialog(ChefActivity.this, t.getMessage());
            }
        });
    }

    /**
    * Receive all orders with a status "cooking" and the chefId of a current Chef
    */
    private void requestSpecificData() {
        //TODO SET CHEF ID
        Call<Map<String, List<OrderDB>>> call = this.retrofit.getChefOrders(this.prefs.getToken(), ORDERS_ISCOOKING_FILTER,
                CHEF_FILTER + this.prefs.getUserId());
        call.enqueue(new Callback<Map<String, List<OrderDB>>>() {
            @Override
            public void onResponse(Call<Map<String, List<OrderDB>>> call, Response<Map<String, List<OrderDB>>> response) {
                updateDataOrders(response.body().get("orders"), 2);
                requestForMenus();
            }

            @Override
            public void onFailure(Call<Map<String, List<OrderDB>>> call, Throwable t) {
                stopRefreshing();
                dialog.createDialog(ChefActivity.this, t.getMessage());
            }
        });
    }

    /**
     * Stops loading dialog
     */
    private void stopRefreshing() {
        this.dialog.hideDialog();
    }

    /**
     * Updates values
     * @param list - received data after the response
     * @param step - indicates whether this method should be invoked for all orders or a current Chef
     */
    private void updateDataOrders(List<OrderDB> list, int step) {
        if (step == 1) {
            this.ordersList.clear();
            this.ordersList.addAll(list);
            this.chosenOrders.clear();
        } else if (step == 2) {
            this.ownChefsList.clear();
            this.ownChefsList.addAll(list);
            this.finishedOrders.clear();
        }
    }

    /**
     * Request menu for displaying appropriate data about the menu ordered
     */
    private void requestForMenus() {
        Call<Map<String, List<MenuDB>>> call = this.retrofit.getCurrentMenu(this.prefs.getToken());
        call.enqueue(new Callback<Map<String, List<MenuDB>>>() {
            @Override
            public void onResponse(Call<Map<String, List<MenuDB>>> call, Response<Map<String, List<MenuDB>>> response) {
                updateDataMenus(response.body().get("menu"));
                stopRefreshing();
            }

            @Override
            public void onFailure(Call<Map<String, List<MenuDB>>> call, Throwable t) {
                stopRefreshing();
                dialog.createDialog(ChefActivity.this, t.getMessage());
            }
        });
    }

    /**
     * Updates data
     * @param listMenus - received data after the request
     */
    private void updateDataMenus(List<MenuDB> listMenus) {
        if(listMenus.size() > 0) {
            this.menuList.clear();
            for (OrderDB order : this.ordersList) {
                for (MenuDB menu : listMenus) {
                    if (order.getMenuId() == menu.getId()) {
                        this.menuList.add(menu);
                    }
                }
            }

            this.ownMenuList.clear();
            for(OrderDB order : this.ownChefsList) {
                for(MenuDB menu : listMenus) {
                    if(order.getMenuId() == menu.getId()) {
                        this.ownMenuList.add(menu);
                    }
                }
            }

            this.chefPagerAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Handles a clicking of a confirm button
     * @param forCooking - indicates the fragment from which the button was clicked
     *                   true - for all orders
     *                   false - for the orders of the current Chef
     */
    @Override
    public void respondAllOrders(boolean forCooking) {
        if(forCooking && this.chosenOrders.size() == 0 || !forCooking && this.finishedOrders.size() == 0) {
            this.dialog.createDialog(this, getString(R.string.dialog_empty_order));
        } else {
            String path = "";
            String status;
            List<OrderDB> tempList;

            if(forCooking) {
                status = this.STATUS_COOKING;
                tempList = new ArrayList<>(this.chosenOrders);
            } else {
                status = this.STATUS_COOKED;
                tempList = new ArrayList<>(this.finishedOrders);
            }

            for (OrderDB order : tempList) {
                path = path.concat(String.valueOf(order.getId())).concat(",");
                order.setStatus(status);
                order.setChefId(this.prefs.getUserId());
            }
            path = path.substring(0, path.length() - 1);

            if(tempList.size() > 1) {
                Call<List<Integer>> call = this.retrofit.cookChangeStatus(path, this.prefs.getToken(), tempList);
                call.enqueue(new Callback<List<Integer>>() {
                    @Override
                    public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                        dialog.createDialog(ChefActivity.this, getString(R.string.dialog_order_complete_title));
                        requestData(false);
                    }

                    @Override
                    public void onFailure(Call<List<Integer>> call, Throwable t) {
                        dialog.createDialog(ChefActivity.this, t.getMessage());
                    }
                });
            } else {
                Call<Integer> call = this.retrofit.cookChangeStatusOne(path, this.prefs.getToken(), tempList);
                call.enqueue(new Callback<Integer>() {
                    @Override
                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                        dialog.createDialog(ChefActivity.this, getString(R.string.dialog_order_complete_title));
                        requestData(false);
                    }

                    @Override
                    public void onFailure(Call<Integer> call, Throwable t) {
                        dialog.createDialog(ChefActivity.this, t.getMessage());
                    }
                });
            }
        }
    }

    /**
     * Handles cancel button to change status back from cooking to in progress
     */
    @Override
    public void respondAllOrdersCancel() {
        if (this.finishedOrders.size() > 0) {
            String path = "";
            List<OrderDB> tempList = new ArrayList<>(this.finishedOrders);
            for (OrderDB order : tempList) {
                path = path.concat(String.valueOf(order.getId())).concat(",");
                order.setStatus(this.STATUS_IN_PROGRESS);
                order.setChefId(0);
            }
            path = path.substring(0, path.length() - 1);

            if (tempList.size() > 1) {
                Call<List<Integer>> call = this.retrofit.cookChangeStatus(path, this.prefs.getToken(), tempList);
                call.enqueue(new Callback<List<Integer>>() {
                    @Override
                    public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                        dialog.createDialog(ChefActivity.this, getString(R.string.dialog_order_complete_title));
                        requestData(false);
                    }

                    @Override
                    public void onFailure(Call<List<Integer>> call, Throwable t) {
                        dialog.createDialog(ChefActivity.this, t.getMessage());
                    }
                });
            } else {
                Call<Integer> call = this.retrofit.cookChangeStatusOne(path, this.prefs.getToken(), tempList);
                call.enqueue(new Callback<Integer>() {
                    @Override
                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                        dialog.createDialog(ChefActivity.this, getString(R.string.dialog_order_complete_title));
                        requestData(false);
                    }

                    @Override
                    public void onFailure(Call<Integer> call, Throwable t) {
                        dialog.createDialog(ChefActivity.this, t.getMessage());
                    }
                });
            }
        }
    }

    /**
     * Handles click on the order to add into the selected list or to remove
     * @param id - clicked line in a adapter
     */
    @Override
    public void respondChefAllAdapter(int id) {
        OrderDB order;
        if(this.viewPager.getCurrentItem() == 0) {
            order = this.ordersList.get(id);
            if(!this.chosenOrders.contains(order)) {
                this.chosenOrders.add(order);
            } else  {
                this.chosenOrders.remove(order);
            }
        } else {
            order = this.ownChefsList.get(id);
            if(!this.finishedOrders.contains(order)) {
                this.finishedOrders.add(order);
            } else {
                this.finishedOrders.remove(order);
            }
        }
    }
}
