package de.h_da.pizzaorder.Activities.Admin;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Adapters.Admin.AdminShowAdapter;
import de.h_da.pizzaorder.Fragments.Admin.AdminChangeFragment;
import de.h_da.pizzaorder.Fragments.Admin.AdminShowFragment;
import de.h_da.pizzaorder.Fragments.Admin.AdminStartFragment;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.ServiceDialog;

public class AdminActivity extends BaseActivity implements AdminStartFragment.Communicator, AdminShowFragment.Communicator,
        AdminShowAdapter.Communicator, AdminChangeFragment.Communicator {

    // Const for evaluating the data in onActivityResult
    private static final int IMG_REQUEST = 777;

    // current type of data (0 - UserDB, 1 - MenuDB, 2 - OrderDB)
    private int typeData;

    // current Data
    private List listData;

    private AdminStartFragment adminStartFragment;

    private AdminShowFragment showFragment;

    private AdminChangeFragment adminChangeFragment;

    private final FragmentManager fragmentManager = getSupportFragmentManager();

    private final ServiceDialog serviceDialog = ServiceDialog.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.adminStartFragment = AdminStartFragment.newInstance();
        this.fragmentManager.beginTransaction().add(R.id.AdminActivity_LinearLayout, this.adminStartFragment).commit();
    }

    @Override
    public void onBackPressed() {
        this.serviceDialog.createDialog(this, null, getString(R.string.dialog_logout_title), null,
                getString(R.string.Generall_Ok_Text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, getString(R.string.Generall_Cancel_Text), null, false, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMG_REQUEST && resultCode == RESULT_OK && data != null) {
            Uri path = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                this.adminChangeFragment.setImage(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Interface implemented for communicating with AdminShowFragment
     * @param data - data to pass( 0 - User, 1 - Menu, 2 - Order)
     * @param listData - list which has to be displayed
     */
    @Override
    public void respondAdminStartFragmentShowFragment(int data, List listData) {
        this.typeData = data;
        this.listData = listData;
        this.showFragment = AdminShowFragment.newInstance(data, (ArrayList) listData);
        this.fragmentManager.beginTransaction().replace(R.id.AdminActivity_LinearLayout, this.showFragment).commit();
    }

    /**
     * Interface implemented for communicating with AdminStartFragment
     * @param data - data to pass( 0 - User, 1 - Menu, 2 - Order)
     */
    @Override
    public void respondAdminStartFragmentCreateNew(int data) {
        this.typeData = data;
        this.adminChangeFragment = AdminChangeFragment.newInstance(data, null);
        this.fragmentManager.beginTransaction().replace(R.id.AdminActivity_LinearLayout, this.adminChangeFragment).commit();
    }

    /**
     * Interface implemented for communicating with AdminShowFragment
     */
    @Override
    public void respondAdminShowFragmentBack() {
        this.fragmentManager.beginTransaction().replace(R.id.AdminActivity_LinearLayout, this.adminStartFragment).commit();
    }

    /**
     * Interface implemented for communicating with AdminShowAdapter
     * @param position - position clicked
     */
    @Override
    public void respondAdminShowAdapter(int position) {
        this.adminChangeFragment = AdminChangeFragment.newInstance(this.typeData, (Parcelable) this.listData.get(position));
        this.fragmentManager.beginTransaction().replace(R.id.AdminActivity_LinearLayout, this.adminChangeFragment).commit();
    }

    /**
     * Interface implemented for communicating with AdminChangeFragment
     */
    @Override
    public void respondAdminChangeFragmentSend() {
        this.fragmentManager.beginTransaction().replace(R.id.AdminActivity_LinearLayout, this.adminStartFragment).commit();
        this.serviceDialog.createDialog(this, getString(R.string.dialog_order_complete_title));
    }

    /**
     * Interface implemented for communicating with AdminChangeFragment
     */
    @Override
    public void respondAdminChangeFragmentBack() {
        this.fragmentManager.beginTransaction().replace(R.id.AdminActivity_LinearLayout, this.adminStartFragment).commit();
    }

    /**
     * Interface implemented for communicating with AdminChangeFragment
     */
    @Override
    public void respondAdminChangeFragmentImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        this.startActivityForResult(intent, IMG_REQUEST);
    }
}
