package de.h_da.pizzaorder.Activities.Customer;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Fragments.Customer.CustomerChangeFragment;
import de.h_da.pizzaorder.Fragments.Customer.CustomerFragmentDescription;
import de.h_da.pizzaorder.Fragments.Customer.CustomerFragmentMain;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Adapters.Customer.MenuAdapter;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener, CustomerFragmentMain.Communicator,
        MenuAdapter.Communicator, CustomerFragmentDescription.Communicator, CustomerChangeFragment.Communicator {

    private FragmentManager fragmentManager = getSupportFragmentManager();

    private CustomerFragmentMain customerFragmentMain;

    private CustomerFragmentDescription customerFragmentDescription;

    private CustomerChangeFragment customerChangeFragment;

    private EditText addressEditText;

    private EditText telephoneEditText;

    private SwipeRefreshLayout swipeRefreshLayout;

    // all menus available
    private List<MenuDB> menuList = new ArrayList<>();

    // menus chosen by user
    private List<MenuDB> usersMenuList = new ArrayList<>();

    private RetrofitInterface retrofit = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);

    private ServiceDialog serviceDialog = ServiceDialog.getInstance();

    private ServiceSharedPrefs prefs = ServiceSharedPrefs.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_customer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.CustomerActivity_Toolbar);
        this.setSupportActionBar(toolbar);
        this.swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.CustomerActivity_SwipeRefreshLayout);
        this.swipeRefreshLayout.setOnRefreshListener(this);
        this.swipeRefreshLayout.setColorSchemeColors(Color.RED, Color.GREEN);

        if (savedInstanceState == null) {
            this.serviceDialog.createLoader(this).show();
            this.requestForMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_first_pizza, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Menu_Settings:
                this.showSettingsActivity();
                return true;
            case R.id.Menu_History:
                this.showHistoryActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        this.backPressedDialog();
    }

    /**
     * Shows HistoryActivity
     */
    private void showHistoryActivity() {
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        this.startActivity(intent);
    }

    /**
     * Shows SettingsActivity
     */
    private void showSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        this.startActivity(intent);
    }

    /**
     * Shows an alert dialog(when back button pressed)
     */
    private void backPressedDialog(){
        this.serviceDialog.createDialog(this, null, getString(R.string.dialog_logout_title), null,
                getString(R.string.Generall_Ok_Text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }, getString(R.string.Generall_Cancel_Text), null, false, true);
    }

    /**
     * Receives info about the current Menu
     */
    private void requestForMenu() {
        Call<Map<String, List<MenuDB>>> call = this.retrofit.getCurrentMenu(this.prefs.getToken());
        call.enqueue(new Callback<Map<String, List<MenuDB>>>() {
            @Override
            public void onResponse(Call<Map<String, List<MenuDB>>> call, Response<Map<String, List<MenuDB>>> response) {
                swipeRefreshLayout.setRefreshing(false);
                serviceDialog.hideDialog();
                List<MenuDB> activeList = new ArrayList<>();
                for (MenuDB menu : response.body().get("menu")) {
                    if (menu.getActive() != 0) {
                        activeList.add(menu);
                    }
                }
                if (activeList.size() != 0) {
                    showCustomerFragmentMain(activeList);
                } else {
                    serviceDialog.createDialog(CustomerActivity.this, getString(R.string.menu_unavailable));
                }
            }
            @Override
            public void onFailure(Call<Map<String, List<MenuDB>>> call, Throwable t) {
                serviceDialog.hideDialog();
                serviceDialog.createDialog(CustomerActivity.this, null, "Error",
                        t.getMessage(), getString(R.string.Generall_Ok_Text), null, null, null, true, true);
            }
        });
    }

    /**
     * Shows fragment for deleting orders
     */
    private void showChangeFragment() {
        if (this.usersMenuList.size() > 0) {
            this.customerChangeFragment = CustomerChangeFragment.newInstance((ArrayList) this.usersMenuList);
            this.fragmentManager.beginTransaction().replace(R.id.CustomerActivity_FrameLayout, this.customerChangeFragment).commit();

            if(this.portraitMode) {
                this.swipeRefreshLayout.setEnabled(false);
            }
        } else {
            this.serviceDialog.createDialog(this, null, null, getString(R.string.dialog_empty_order),
                    this.getString(R.string.Generall_Ok_Text), null, null, null, false, false);
        }
    }

    /**
     * Shows a fragment with a data received from MenuDatabase
     * @param list - data received from MenuDatabase
     */
    private void showCustomerFragmentMain(List<MenuDB> list) {
        this.menuList = list;

        if(this.customerFragmentMain ==  null) {
            this.customerFragmentMain = CustomerFragmentMain.
                    newInstance((ArrayList<MenuDB>)CustomerActivity.this.menuList,
                            (ArrayList<MenuDB>) this.usersMenuList);
            if (this.portraitMode) {
                fragmentManager.beginTransaction().add(R.id.CustomerActivity_FrameLayout, customerFragmentMain).commit();
            } else {
                fragmentManager.beginTransaction().add(R.id.CustomerActivity_FrameLayoutMain, customerFragmentMain).commit();
            }
        } else {
            this.customerFragmentMain.notifyFragmentData(list);
        }

    }

    /**
     * Makes an order with chosen pizzas
     */
    private void makeOrder() {
        if (this.usersMenuList.size() > 0) {
            this.serviceDialog.createDialog(this, this.inflateViewDialog(), null, null, this.getString(R.string.Generall_Ok_Text), this.okDialog,
                    this.getString(R.string.Generall_Cancel_Text), null, false, false);
        } else {
            this.serviceDialog.createDialog(this, null, null, getString(R.string.dialog_empty_order),
            this.getString(R.string.Generall_Ok_Text), null, null, null, false, false);
        }
    }

    /**
     * handles ok button in dialog
     */
    private DialogInterface.OnClickListener okDialog = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            proceedOrder();
        }
    };

    /**
     * Shows a view with a address and telephone
     * @return - inflated view
     */
    private View inflateViewDialog() {
        String[] addressTelephone = this.prefs.getAddressTelephone();
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_address_telephone, null);
        this.addressEditText = (EditText) view.findViewById(R.id.DIalogAddressTelephone_Address_EditText);
        this.telephoneEditText = (EditText) view.findViewById(R.id.DIalogAddressTelephone_Telephone_EditText);
        this.addressEditText.setText(addressTelephone[0]);
        this.telephoneEditText.setText(addressTelephone[1]);
        this.telephoneEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(telephoneEditText.getWindowToken(), 0);
                    proceedOrder();
                    serviceDialog.hideDialog();
                    return true;
                }
                return false;
            }
        });
        return view;
    }

    /**
     * Handles first step of the process of the order
     */
    private void proceedOrder() {
        String address = this.addressEditText.getText().toString();
        String telephone = this.telephoneEditText.getText().toString();
        if (!address.equals("") && !telephone.equals("")) {
            this.serviceDialog.createLoader(this).show();
            this.prefs.saveAddressTelephone(address, telephone);
            List<OrderDB> orderList = new ArrayList<>();
            for (MenuDB menu : this.usersMenuList) {
                //TODO Fill with the appropriate customerId
                orderList.add(new OrderDB(this.prefs.getUserId(),menu.getId(), "in progress", address, telephone));
            }

            if (orderList.size() > 1) {
                this.makeSomeOrders(orderList);
            } else {
                this.makeOneOrder(orderList);
            }
        }
    }

    /**
     * Makes a request when user chose only 1 pizza
     */
    private void makeOneOrder(List<OrderDB> orderList) {
        Call <Integer> call = this.retrofit.makeOneOrder(this.prefs.getToken(), orderList);
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                showResultDialog(true, null);
                //TODO We receive an int value for created order
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                showResultDialog(false, t.getMessage());
            }
        });
    }

    /**
     * Makes an order when user chose more than one pizza
     */
    private void makeSomeOrders(List<OrderDB> orderList) {
        Call<List<Integer>> call = retrofit.makeSomeOrders(this.prefs.getToken(), orderList);
        call.enqueue(new Callback<List<Integer>>() {
            @Override
            public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                showResultDialog(true, null);
                //TODO We receive An Array of Integers with a new IDs for users order
            }

            @Override
            public void onFailure(Call<List<Integer>> call, Throwable t) {
                showResultDialog(false, t.getMessage());
            }
        });
    }

    /**
     * Shows a dialog after the order done
     * @param result - indicates whether the request was successful or not
     * @param message - message to display(only when an error)
     */
    private void showResultDialog(boolean result, @Nullable String message) {
        this.serviceDialog.hideDialog();
        if (result) {
            this.serviceDialog.createDialog(this, null, this.getString(R.string.dialog_order_complete_title),
                    this.getString(R.string.dialog_order_complete_message), null, null, null, null, true, true);
            this.usersMenuList.clear();
            this.customerFragmentMain.resetNumber();
            //TODO Set to 0 the value in the bracket's order
        } else {
            this.serviceDialog.createDialog(this, null, null, message, null, null, null, null, true, true);
        }
    }

    /**
     * Shows a description of the pizza, on which user clicked
     * @param position - a position of clicked element in menu adapter (the same for a menu list)
     */
    @Override
    public void respondMenuAdapter(int position) {
        this.customerFragmentDescription = CustomerFragmentDescription.newInstance(this.menuList.get(position), false);
        this.fragmentManager.beginTransaction().replace(R.id.CustomerActivity_FrameLayout, this.customerFragmentDescription).commit();
        if(this.portraitMode) {
            this.swipeRefreshLayout.setEnabled(false);
        }
    }

    /**
     * Shows again main fragment with a menu list
     */
    @Override
    public void respondCustomerFragmentDescriptionBack() {
        if (this.portraitMode) {
            this.fragmentManager.beginTransaction().replace(R.id.CustomerActivity_FrameLayout, this.customerFragmentMain).commit();
            this.swipeRefreshLayout.setEnabled(true);
        }
        this.customerFragmentMain.updateNumber(this.usersMenuList.size());
    }

    /**
     * Adds the current pizza for buying
     * @param menu - chosen menu
     */
    @Override
    public void respondCustomerFragmentDescriptionAdd(MenuDB menu) {
        this.usersMenuList.add(menu);
        this.respondCustomerFragmentDescriptionBack();
    }

    /**
     * Makes an order(when user presses "order" button)
     * @param menu - current pizza for ordering
     */
    @Override
    public void respondCustomerFragmentDescriptionOrder(MenuDB menu) {
        this.usersMenuList.clear();
        this.usersMenuList.add(menu);
        this.respondCustomerFragmentDescriptionBack();
        this.makeOrder();
    }

    /**
     * handles order button
     */
    @Override
    public void respondCustomerFragmentMainMakeOrder() {
        this.makeOrder();
    }

    /**
     * Shows fragment for editing
     */
    @Override
    public void respondCustomerFragmentMainChangeFragment() {
        this.showChangeFragment();
    }

    /**
     * Updates received data
     * @param list - received data after the response
     */
    @Override
    public void respondCustomerChangeFragmentRefresh(List<MenuDB> list) {
        if(list.size() < this.usersMenuList.size()) {
            this.usersMenuList.clear();
            this.usersMenuList.addAll(list);
        }
        this.respondCustomerFragmentDescriptionBack();
    }

    @Override
    public void onRefresh() {
        if(!this.portraitMode || this.customerFragmentMain.isVisible()) {
            this.swipeRefreshLayout.setRefreshing(true);
            this.requestForMenu();
        }
    }
}

