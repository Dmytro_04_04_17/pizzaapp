package de.h_da.pizzaorder.Activities.Custom;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import java.util.List;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalInfoEditActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar toolbar;

    private EditText etCurrentPass;
    private EditText etNewPass;
    private EditText etReNewPass;
    private EditText etName;
    private EditText etFamily;
    private EditText etTelephone;
    private EditText etEmail;
    private EditText etAddress;

    //Current info
    private String currentName = "";
    private String currentFamily = "";
    private String currentTelephone = "";
    private String currentEmail = "";
    private String currentAddress = "";

    private ScrollView svScrollview;

    private Button bChangePassword;
    private Button bChangeInfo;
    private ServiceSharedPrefs sharedPrefs = ServiceSharedPrefs.getInstance();

    private RetrofitInterface retrofitInterfaceObject = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);
    private ValidatePassword validatorPassword;
    private ValidateInfo validateInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        View view = LayoutInflater.from(this).inflate(R.layout.activity_personal_info_edit, null);
        super.onCreate(savedInstanceState);
        setContentView(view);
        initializeUI(view);
        setListeners();
        setValidation();
        fillFields();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.PersonalInfoActivity_B_changePassword:
                disableChnagePasswordButton();
                validatorPassword.validate();
                break;

            case R.id.PersonalInfoActivity_B_changeInfo:
                disableChangeInfoButton();
                validateInfo.validate();
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initializeUI(View view) {
        this.toolbar = (Toolbar) findViewById(R.id.PersonalInfoActivity_T_toolbar);
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        /*
        *  Make back arrow white
        * */
        final Drawable upArrow = ContextCompat.getDrawable(this, R.drawable.ic_menu_back);
        upArrow.setColorFilter(ContextCompat.getColor(this,R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);


        this.etCurrentPass = (EditText) view.findViewById(R.id.PersonalInfoActivity_ET_currentpass);
        this.etNewPass = (EditText) view.findViewById(R.id.PersonalInfoActivity_ET_newpass);
        this.etReNewPass = (EditText) view.findViewById(R.id.PersonalInfoActivity_ET_renewpass);
        this.etName = (EditText) view.findViewById(R.id.PersonalInfoActivity_ET_name);
        this.etFamily = (EditText) view.findViewById(R.id.PersonalInfoActivity_ET_family);
        this.etEmail = (EditText) view.findViewById(R.id.PersonalInfoActivity_ET_email);
        this.etTelephone = (EditText) view.findViewById(R.id.PersonalInfoActivity_ET_telephone);
        this.etAddress = (EditText) view.findViewById(R.id.PersonalInfoActivity_ET_address);

        this.svScrollview = (ScrollView) view.findViewById(R.id.PersonalInfoActivity_SV_scrollview);

        this.bChangePassword = (Button) view.findViewById(R.id.PersonalInfoActivity_B_changePassword);
        this.bChangeInfo = (Button) view.findViewById(R.id.PersonalInfoActivity_B_changeInfo);
    }

    private void setListeners() {
        this.bChangeInfo.setOnClickListener(this);
        this.bChangePassword.setOnClickListener(this);

        this.etAddress.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validateInfo.validate();
                    return true;
                }
                return false;
            }
        });

        this.etReNewPass.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validatorPassword.validate();
                    return true;
                }
                return false;
            }
        });
    }

    private void setValidation() {
        validatorPassword = new ValidatePassword(etCurrentPass, etNewPass, etReNewPass, new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                changePassword();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError oneError : errors) {
                    View errorView = oneError.getView();
                    String message = oneError.getCollatedErrorMessage(PersonalInfoEditActivity.this);
                    if (errorView instanceof EditText) {
                        ((EditText) errorView).setError(message);
                    }
                }
                enableChangePasswordButton();
            }
        });

        validateInfo = new ValidateInfo(etName, etFamily, etTelephone, etEmail, etAddress, new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                updateInfo();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError oneError : errors) {
                    View errorView = oneError.getView();
                    String message = oneError.getCollatedErrorMessage(PersonalInfoEditActivity.this);
                    if (errorView instanceof EditText) {
                        ((EditText) errorView).setError(message);
                    }
                }
                enableChangeInfoButton();
            }
        });
    }

    private void changePassword() {

        if (sharedPrefs.getPassword().equals(etNewPass.getText().toString())) {
            ServiceDialog.getInstance().createDialog(PersonalInfoEditActivity.this, "Please write a new password!")
                    .show();
            enableChangePasswordButton();
        } else {
            if (sharedPrefs.getPassword().equals(etCurrentPass.getText().toString())) {
                Call<Integer> call = retrofitInterfaceObject
                        .changePassword(sharedPrefs.getUserId()
                                , sharedPrefs.getToken()
                                , etNewPass.getText().toString());

                call.enqueue(new Callback<Integer>() {
                    @Override
                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                        enableChangePasswordButton();
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                sharedPrefs.setPassword(etNewPass.getText().toString());
                                ServiceDialog.getInstance()
                                        .createDialog(PersonalInfoEditActivity.this, "Your password is changed!")
                                        .show();
                                svScrollview.fullScroll(View.FOCUS_UP);
                            } else {
                                ServiceDialog.getInstance()
                                        .createDialog(PersonalInfoEditActivity.this, "Your password is not changed!")
                                        .show();

                                svScrollview.fullScroll(View.FOCUS_UP);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Integer> call, Throwable t) {
                        enableChangePasswordButton();
                        ServiceDialog.getInstance()
                                .createDialog(PersonalInfoEditActivity.this, "Error:\n" + t.getMessage())
                                .show();
                        svScrollview.fullScroll(View.FOCUS_UP);
                    }
                });
            } else {
                enableChangePasswordButton();
                ServiceDialog.getInstance()
                        .createDialog(PersonalInfoEditActivity.this, "Your current password doesn't match!")
                        .show();
                svScrollview.fullScroll(View.FOCUS_UP);
            }
        }
    }

    private void updateInfo() {
        if (isInfoChanged()) {
            Call<Integer> call = retrofitInterfaceObject
                    .updateInfo(sharedPrefs.getUserId()
                            , sharedPrefs.getToken()
                            , etName.getText().toString()
                            , etFamily.getText().toString()
                            , etEmail.getText().toString()
                            , etTelephone.getText().toString()
                            , etAddress.getText().toString());

            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    enableChangeInfoButton();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            sharedPrefs.setPassword(etNewPass.getText().toString());
                            ServiceDialog.getInstance().createDialog(PersonalInfoEditActivity.this, "Your info is updated!").show();
                            updateCurrentInfo();
                            svScrollview.fullScroll(View.FOCUS_UP);
                        } else {
                            ServiceDialog.getInstance().createDialog(PersonalInfoEditActivity.this, "Your info is not updated!").show();
                            svScrollview.fullScroll(View.FOCUS_UP);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    enableChangeInfoButton();
                    ServiceDialog.getInstance().createDialog(PersonalInfoEditActivity.this, "Error:\n" + t.getMessage()).show();
                    svScrollview.fullScroll(View.FOCUS_UP);
                }
            });
        } else {
            enableChangeInfoButton();
            ServiceDialog.getInstance().createDialog(PersonalInfoEditActivity.this, "Please change something!").show();
        }
    }

    private void fillFields() {
        Call<UserDB> call = retrofitInterfaceObject.getOneUser(sharedPrefs.getUserId(), sharedPrefs.getToken());
        call.enqueue(new Callback<UserDB>() {
            @Override
            public void onResponse(Call<UserDB> call, Response<UserDB> response) {
                if (response.isSuccessful() && response.body() != null) {
                    currentName = response.body().getName();
                    currentFamily = response.body().getFamily();
                    currentEmail = response.body().getEmail();
                    currentTelephone = response.body().getTelephone();
                    currentAddress = response.body().getAddress();

                    etName.setText(currentName);
                    etFamily.setText(currentFamily);
                    etEmail.setText(currentEmail);
                    etTelephone.setText(currentTelephone);
                    etAddress.setText(currentAddress);


                }
            }

            @Override
            public void onFailure(Call<UserDB> call, Throwable t) {
                ServiceDialog.getInstance()
                        .createDialog(PersonalInfoEditActivity.this, "Error:\n" + t.getMessage())
                        .show();
                svScrollview.fullScroll(View.FOCUS_UP);
            }
        });
    }

    private boolean isInfoChanged() {
        if (!etName.getText().toString().equals(currentName)
                || !etFamily.getText().toString().equals(currentFamily)
                || !etEmail.getText().toString().equals(currentEmail)
                || !etTelephone.getText().toString().equals(currentTelephone)
                || !etAddress.getText().toString().equals(currentAddress))
            return true;
        else
            return false;
    }

    private void updateCurrentInfo() {
        currentName = etName.getText().toString();
        currentFamily = etFamily.getText().toString();
        currentEmail = etEmail.getText().toString();
        currentTelephone = etTelephone.getText().toString();
        currentAddress = etAddress.getText().toString();
    }

    private void disableChnagePasswordButton() {
        bChangePassword.setEnabled(false);
    }

    private void enableChangePasswordButton() {
        bChangePassword.setEnabled(true);
    }

    private void disableChangeInfoButton() {
        bChangeInfo.setEnabled(false);
    }

    private void enableChangeInfoButton() {
        bChangeInfo.setEnabled(true);
    }

    class ValidatePassword {
        @NotEmpty(trim = true)
        @Length(max = 45, message = "Maximun lenght is 45 characters")
        private EditText etCurrentPass;

        @NotEmpty(trim = true)
        @Password
        @Length(min = 8, max = 45, message = "Password should be between 8 to 45 characters")
        private EditText etNewPass;

        @NotEmpty(trim = true)
        @ConfirmPassword
        @Length(min = 8, max = 45, message = "Password should be between 8 to 45 characters")
        private EditText etReNewPass;

        private Validator validator;

        public ValidatePassword(EditText currentPass, EditText newPass, EditText reNewPass, Validator.ValidationListener listener) {
            etCurrentPass = currentPass;
            etNewPass = newPass;
            etReNewPass = reNewPass;

            validator = new Validator(this);
            validator.setValidationListener(listener);
        }

        public void validate() {
            validator.validate();
        }
    }

    class ValidateInfo {
        Validator validator;
        @NotEmpty(trim = true)
        @Length(min = 2, max = 40, message = "Name should be between 2 to 40 characters")
        private EditText etName;
        @NotEmpty(trim = true)
        @Length(min = 1, max = 40, message = "Family should be between 1 to 40 characters")
        private EditText etFamily;
        @NotEmpty(trim = true)
        @Length(min = 1, max = 13, message = "Telephone should be between 1 to 13 digit")
        private EditText etTelephone;
        @NotEmpty(trim = true)
        @Length(max = 40, message = "Maximun lenght is 40 characters")
        @Email
        private EditText etEmail;
        @NotEmpty(trim = true)
        @Length(min = 1, max = 500, message = "Maximun lenght is 500 characters")
        private EditText etAddress;

        public ValidateInfo(EditText name
                , EditText family
                , EditText telephone
                , EditText email
                , EditText address
                , Validator.ValidationListener listener) {
            etName = name;
            etFamily = family;
            etTelephone = telephone;
            etEmail = email;
            etAddress = address;

            validator = new Validator(this);
            validator.setValidationListener(listener);
        }

        public void validate() {
            validator.validate();
        }

    }
}
