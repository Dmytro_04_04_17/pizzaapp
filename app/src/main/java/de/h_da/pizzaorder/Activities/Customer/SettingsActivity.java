package de.h_da.pizzaorder.Activities.Customer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Activities.Custom.LoginActivity;
import de.h_da.pizzaorder.Activities.Custom.PersonalInfoEditActivity;
import de.h_da.pizzaorder.Adapters.Customer.SettingsAdapter;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;

public class SettingsActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    private ListView listView;

    private SettingsAdapter settingsAdapter;

    private Toolbar toolbar;

    private ServiceSharedPrefs sharedPrefs = ServiceSharedPrefs.getInstance();

    private List<String> data;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_settings);
        this.initializeUI();
        this.setListeners();
        this.setData();
    }

    /**
     * Initializes widgets
     */
    private void initializeUI() {
        this.listView = (ListView) this.findViewById(R.id.SettingsActivity_ListView);
        this.toolbar = (Toolbar) this.findViewById(R.id.SettingsActivity_Toolbar);
    }

    /**
     * Sets listeners to widgets
     */
    private void setListeners() {
        this.listView.setOnItemClickListener(this);
    }

    /**
     * Sets data
     */
    private void setData() {
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.data = new ArrayList<>(Arrays.asList(
                getString(R.string.info_activity_settings),
                getString(R.string.map_activity_settings),
                getString(R.string.profile_data_activity_settings) ,
                getString(R.string.delete_password_activity_settings),
                getString(R.string.logout_activity_settings)
        ));
        this.settingsAdapter = new SettingsAdapter(data);
        this.listView.setAdapter(settingsAdapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch(position) {
            case 0:
                //TODO Activity with Info about the pizzeria
                break;
            case 1:
                Uri mapUri = Uri.parse("geo:0,0?q=" + Uri.encode("Schöfferstraße 3, 64295 Darmstadt"));
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    this.startActivity(mapIntent);
                }
                break;
            case 2:
                this.startActivity(new Intent(SettingsActivity.this,PersonalInfoEditActivity.class));
                break;
            case 3:
                this.sharedPrefs.deleteLogPass();
                break;
            case 4:
                Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                this.sharedPrefs.setSuccessfulLogin(false);
                this.startActivity(intent);
                break;
            default:
                throw new IllegalStateException("Unexpected value");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
