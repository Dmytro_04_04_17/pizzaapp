package de.h_da.pizzaorder.Models;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;


/*
* This class is used for serialize and deserialize the data that API will return to any authentication query
* the data that API send contains these :
* error , login , user , type , id
* */
public class ApiDB {
    @Nullable
    @SerializedName("error")
    private String error;

    @SerializedName("login")
    private String login;

    @SerializedName("user")
    private String user;

    @SerializedName("type")
    private String type;

    @SerializedName("id")
    private String id;


    public ApiDB(String error, String login, String user) {
        this.error = error;
        this.login = login;
        this.user = user;
    }

    public ApiDB(String error) {
        this.error = error;
    }

    public String getUsername() {
        return user;
    }

    public void setUsername(String user) {
        this.user = user;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String toString(){
        return "error : "+error+" login : "+login +" username : "+user;
    }
}
