package de.h_da.pizzaorder.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Model which represents menu
 */
public class MenuDB implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("ingredients")
    private String ingredients;

    @SerializedName("weight")
    private double weight;

    @SerializedName("price")
    private double price;

    @SerializedName("image")
    private String image;

    @SerializedName("active")
    private int active;

    public MenuDB(String name, String ingredients, double weight, double price, String image, int active) {
        this.name = name;
        this.ingredients = ingredients;
        this.weight = weight;
        this.price = price;
        this.image = image;
        this.active = active;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    protected MenuDB(Parcel in) {
        id = in.readInt();
        name = in.readString();
        ingredients = in.readString();
        weight = in.readDouble();
        price = in.readDouble();
        image = in.readString();
        active = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(ingredients);
        dest.writeDouble(weight);
        dest.writeDouble(price);
        dest.writeString(image);
        dest.writeInt(active);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MenuDB> CREATOR = new Parcelable.Creator<MenuDB>() {
        @Override
        public MenuDB createFromParcel(Parcel in) {
            return new MenuDB(in);
        }

        @Override
        public MenuDB[] newArray(int size) {
            return new MenuDB[size];
        }
    };
}
