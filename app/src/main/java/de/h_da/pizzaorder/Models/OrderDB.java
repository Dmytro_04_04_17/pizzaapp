package de.h_da.pizzaorder.Models;

/**
 * Model which represents order
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OrderDB implements Parcelable {

    @SerializedName("id")
    private int id;
    @SerializedName("customerId")
    private int customerId;
    @SerializedName("deliveryId")
    private int deliveryId;
    @SerializedName("chefId")
    private int chefId;
    @SerializedName("menuId")
    private int menuId;
    @SerializedName("status")
    private String status;
    @SerializedName("customerAddress")
    private String customerAddress;
    @SerializedName("customerTelephone")
    private String customerTelephone;
    @SerializedName("time")
    private String date;
    @SerializedName("customerFeedback")
    private String customerFeedback;

    public OrderDB (int customerId, int menuId, String status, String customerAddress, String customerTelephone) {
        this.customerId = customerId;
        this.menuId = menuId;
        this.status = status;
        this.customerAddress = customerAddress;
        this.customerTelephone = customerTelephone;
    }

    public OrderDB(String status) {
        this.status = status;
    }

    public OrderDB (int customerId, int chefId, int menuId, String status, String customerAddress, String customerTelephone,
                    String date, String customerFeedback) {
        this.customerId = customerId;
        this.chefId = chefId;

        this.menuId = menuId;
        this.status = status;
        this.customerAddress = customerAddress;
        this.customerTelephone = customerTelephone;
        this.date = date;
        this.customerFeedback = customerFeedback;


    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getChefId() {
        return chefId;
    }

    public void setChefId(int chefId) {
        this.chefId = chefId;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerTelephone() {
        return customerTelephone;
    }

    public void setCustomerTelephone(String customerTelephone) {
        this.customerTelephone = customerTelephone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustomerFeedback() {
        return customerFeedback;
    }

    public void setCustomerFeedback(String customerFeedback) {
        this.customerFeedback = customerFeedback;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(int deliveryid) {
        this.deliveryId = deliveryid;
    }

    protected OrderDB(Parcel in) {
        id = in.readInt();
        customerId = in.readInt();
        deliveryId = in.readInt();
        chefId = in.readInt();
        menuId = in.readInt();
        status = in.readString();
        customerAddress = in.readString();
        customerTelephone = in.readString();
        date = in.readString();
        customerFeedback = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(customerId);
        dest.writeInt(deliveryId);
        dest.writeInt(chefId);
        dest.writeInt(menuId);
        dest.writeString(status);
        dest.writeString(customerAddress);
        dest.writeString(customerTelephone);
        dest.writeString(date);
        dest.writeString(customerFeedback);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<OrderDB> CREATOR = new Parcelable.Creator<OrderDB>() {
        @Override
        public OrderDB createFromParcel(Parcel in) {
            return new OrderDB(in);
        }

        @Override
        public OrderDB[] newArray(int size) {
            return new OrderDB[size];
        }
    };
}
