package de.h_da.pizzaorder.Models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Model which represents order
 */
public class UserDB implements Parcelable {

    @Nullable
    @SerializedName("id")
    private Integer id;

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("type")
    private String type;

    @SerializedName("name")
    private String name;

    @SerializedName("family")
    private String family;

    @SerializedName("address")
    private String address;

    @SerializedName("email")
    private String email;

    @SerializedName("telephone")
    private String telephone;

    @SerializedName("locked")
    private int locked;

    public UserDB(Integer id, String username, String password, String type, String name, String family, String address, String email, String telephone, int locked) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.type = type;
        this.name = name;
        this.family = family;
        this.address = address;
        this.email = email;
        this.telephone = telephone;
        this.locked = locked;
    }

    public UserDB(String username, String password, String type, String name, String family, String address, String email, String telephone, int locked) {
        this.username = username;
        this.password = password;
        this.type = type;
        this.name = name;
        this.family = family;
        this.address = address;
        this.email = email;
        this.telephone = telephone;
        this.locked = locked;
    }

    //Constructor for using in login procedure
    public UserDB(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UserDB(String username, String password, String type) {
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    protected UserDB(Parcel in) {
        id = in.readInt();
        username = in.readString();
        password = in.readString();
        type = in.readString();
        name = in.readString();
        family = in.readString();
        address = in.readString();
        email = in.readString();
        telephone = in.readString();
        locked = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(username);
        dest.writeString(password);
        dest.writeString(type);
        dest.writeString(name);
        dest.writeString(family);
        dest.writeString(address);
        dest.writeString(email);
        dest.writeString(telephone);
        dest.writeInt(locked);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserDB> CREATOR = new Parcelable.Creator<UserDB>() {
        @Override
        public UserDB createFromParcel(Parcel in) {
            return new UserDB(in);
        }

        @Override
        public UserDB[] newArray(int size) {
            return new UserDB[size];
        }
    };
}
