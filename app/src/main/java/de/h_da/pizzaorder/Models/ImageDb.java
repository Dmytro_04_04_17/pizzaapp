package de.h_da.pizzaorder.Models;

import com.google.gson.annotations.SerializedName;


/*
* This class is used for serialize and deserialize the data that API will return to Image uploading query
* the data that API send contains these :
* title , image , response , path
* */
public class ImageDb {

    @SerializedName("title")
    private String title;

    @SerializedName("image")
    private String image;

    @SerializedName("response")
    private String response;

    @SerializedName("path")
    private String path;

    public String getResponse() {
        return response;
    }

    public String getPath() {
        return path;
    }
}
