package de.h_da.pizzaorder.Utilities;


import java.util.List;
import java.util.Map;

import de.h_da.pizzaorder.Models.ApiDB;
import de.h_da.pizzaorder.Models.ImageDb;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.Models.UserDB;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


public interface RetrofitInterface {

    @POST("users")
    Call<Integer> createUser(@Query("csrf") String token, @Body UserDB userDB);

    @PUT("users/{id}")
    Call<Integer> changeUser(@Path("id") String id, @Query("csrf")String token, @Body UserDB userDB);

    @POST("menu")
    Call<Integer> createMenu(@Query("csrf") String token, @Body MenuDB menuDB);

    @PUT("menu/{id}")
    Call<Integer> changeMenu(@Path("id") String id, @Query("csrf")String token, @Body MenuDB menuDB);

    @POST("orders")
    Call<Integer> createOrder(@Query("csrf") String token, @Body OrderDB orderDB);

    @POST("orders")
    Call<List<Integer>> makeSomeOrders(@Query("csrf") String token, @Body List<OrderDB> list);

    @POST("orders")
    Call<Integer> makeOneOrder(@Query("csrf") String token, @Body List<OrderDB> list);

    @GET("menu?transform=1")

    Call<Map<String, List<MenuDB>>> getCurrentMenu(@Query("csrf")String token);

    @GET("users?transform=1")
    Call<Map<String, List<UserDB>>> getAllUsers(@Query("csrf")String token);

    @GET("orders?transform=1")
    Call<Map<String, List<OrderDB>>> getAllOrders(@Query("csrf")String token);


    //all the GET function should have csrf and transform as Query input


    /*
    * Used in delivery flow
    * */
    @GET("users/{id}")
    Call<UserDB> getOneUser(@Path("id") int id, @Query("csrf") String token);

    @FormUrlEncoded
    @PUT("orders/{id}")
    Call<Integer> setDeliveryId(@Path("id") int id, @Query("csrf") String token, @Field("deliveryId") int deliveryId);

    @FormUrlEncoded
    @PUT("orders/{id}")
    Call<Integer> setStatus(@Path("id") int id, @Query("csrf") String token, @Field("status") String status);


    @GET("menu/{ids}")
    Call<MenuDB[]> getOrderMenus(@Path("ids") String ids, @Query("csrf") String token);

    //For login procedure
    @POST("./")
    Call<ApiDB> checkCredentials(@Body UserDB user);

    //For uploading an image
    @FormUrlEncoded
    @POST("./")
    Call<ImageDb> uploadImage(@Field("upload") boolean upload
                            , @Field("token") String token
                            , @Field("user") String user
                            , @Field("title") String title
                            , @Field("image") String image);

    //It gets the orders based on the @param data
    @GET("orders?transform=1")
    Call<Map<String, List<OrderDB>>> getUserOrders(@Query("csrf") String token, @Query("filter") String data);

    @GET("menu/{id}")
    Call<MenuDB> getCurrentMenu(@Path("id") int id, @Query("csrf") String token);

    @GET("menu?transform=1")
    Call<Map<String, List<MenuDB>>> getAllMenus(@Query("filter[]") List<String> filters, @Query("csrf")String token);

    @FormUrlEncoded
    @PUT("orders/{id}")
    Call<Integer> sendFeedback(@Path("id") int id, @Query("csrf") String token, @Field("customerFeedback") String feedback);

    @FormUrlEncoded
    @PUT("users/{id}")
    Call<Integer> changePassword(@Path("id") int id, @Query("csrf") String token, @Field("password") String password);

    @FormUrlEncoded
    @PUT("users/{id}")
    Call<Integer> updateInfo(@Path("id") int id, @Query("csrf") String token
            , @Field("name") String name
            , @Field("family") String family
            , @Field("email") String email
            , @Field("telephone") String telephone
            , @Field("address") String address
            );

    @GET("orders?transform=1")
    Call <Map<String, List<OrderDB>>> getChefOrders(@Query("csrf")String token, @Query("filter[]") String firstData,
                                                    @Query("filter[]") String secondData);


    @PUT("orders/{id}")
    Call<List<Integer>> cookChangeStatus(@Path("id") String id, @Query("csrf")String token, @Body List<OrderDB> list);

    @PUT("orders/{id}")
    Call<Integer> cookChangeStatusOne(@Path("id") String id, @Query("csrf")String token, @Body List<OrderDB> list);
}
