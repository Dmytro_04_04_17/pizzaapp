package de.h_da.pizzaorder.Utilities;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.h_da.pizzaorder.Models.OrderDB;

/**
 * Created by AEF on 6/1/2017.
 */

public class OrdersHelper {

    private static OrdersHelper OrdersHelper;

    private ServiceSharedPrefs prefs = ServiceSharedPrefs.getInstance();

    public static OrdersHelper getInstance() {
        if (OrdersHelper == null) {
            OrdersHelper = new OrdersHelper();
        }
        return OrdersHelper;
    }


    /*
    * used for categorizing the cooked and inprogress based on the customerId
    *
    * @param allTodayOrders : it is the mTodayOrdersList
    * @return A hashmap that contains the ready and notready orders and stored by the customerId
    * */

    public HashMap<Integer, ArrayList<OrderDB>> getCategorizedOrdersByCustomerId(List<OrderDB> allTodayOrders) {

        HashMap<Integer, ArrayList<OrderDB>> map = new HashMap<>();

        for (OrderDB order : allTodayOrders) {
            // If the customer does not exist in the hashmap
            if (!map.containsKey(order.getCustomerId())) {
                ArrayList<OrderDB> listInHash = new ArrayList<>();
                listInHash.add(order);
                map.put(order.getCustomerId(), listInHash);
            } else {
                // add the product to the arraylist that corresponds to the key
                ArrayList<OrderDB> listInHash = map.get(order.getCustomerId());
                listInHash.add(order);
            }
        }
        return map;
    }

    /*
    * used for categorizing the cooked and inprogress based on the date
    *
    * @param allTodayOrders : it is the mTodayOrdersList
    * @return A hashmap that contains the ready and notready orders and stored by the Date
    * */

    public HashMap<String, ArrayList<OrderDB>> getCategorizedOrdersByDateTime(List<OrderDB> allTodayOrders) {

        HashMap<String, ArrayList<OrderDB>> map = new HashMap<>();

        for (OrderDB order : allTodayOrders) {
            // If the customer does not exist in the hashmap
            if (!map.containsKey(order.getDate())) {
                ArrayList<OrderDB> listInHash = new ArrayList<>();
                listInHash.add(order);
                map.put(order.getDate(), listInHash);
            } else {
                // add the item to the arraylist that corresponds to the key
                ArrayList<OrderDB> listInHash = map.get(order.getDate());
                listInHash.add(order);
            }
        }
        return map;
    }

    /*
    * get the ready orders by passing the mCategorizedOrdersByCustomerId
    * */
    public HashMap<Integer, ArrayList<OrderDB>> getReadyOrders
    (HashMap<Integer, ArrayList<OrderDB>> categorizedOrders) {
        HashMap<Integer, ArrayList<OrderDB>> map = new HashMap<>();

        for (Map.Entry<Integer, ArrayList<OrderDB>> wholeCustomerOrder : categorizedOrders.entrySet()) {
            ArrayList<OrderDB> customerOrdersList = wholeCustomerOrder.getValue();
            boolean ready = true;
            for (OrderDB eachOrder : customerOrdersList) {
                if (!eachOrder.getStatus().equals("cooked")) {
                    ready = false;
                }
            }
            if (ready) {
                map.put(wholeCustomerOrder.getKey(), customerOrdersList);
            }
        }
        return map;
    }


    /*
    * get the ready orders by passing the mCategorizedOrdersByDateTime
    * */
    public HashMap<String, ArrayList<OrderDB>> getReadyOrdersWhithoutDeliveryId
    (HashMap<String, ArrayList<OrderDB>> categorizedOrders) {
        HashMap<String, ArrayList<OrderDB>> map = new HashMap<>();

        for (Map.Entry<String, ArrayList<OrderDB>> wholeCustomerOrderInOneDateTime : categorizedOrders.entrySet()) {
            ArrayList<OrderDB> customerOrdersListInOneDateTime = wholeCustomerOrderInOneDateTime.getValue();
            boolean ready = true;
            for (OrderDB eachOrder : customerOrdersListInOneDateTime) {
                if (!eachOrder.getStatus().equals("cooked")) {
                    ready = false;
                }
            }
            if (ready) {
                //TODO : I check with the first row. there should be a better way
                //double check - maybe the input order map contains some orders that has deliveryId
                if (wholeCustomerOrderInOneDateTime.getValue().get(0).getDeliveryId() == 0) {
                    map.put(wholeCustomerOrderInOneDateTime.getKey(), customerOrdersListInOneDateTime);
                }
            }
        }
        return map;
    }


    /*
    * get my tasks done by passing the allOrders
    * */
    public HashMap<String, ArrayList<OrderDB>> getMyTasks(HashMap<String, ArrayList<OrderDB>> allTodayOrders) {

        HashMap<String, ArrayList<OrderDB>> myTasks = new HashMap<>();

        for (Map.Entry<String, ArrayList<OrderDB>> oneOrder : allTodayOrders.entrySet()) {
            Log.d("xxxx","one order at "+oneOrder.getKey());
            ArrayList<OrderDB> customerOrdersList = oneOrder.getValue();
            boolean mine = true;
            for (OrderDB eachRowInOrder : customerOrdersList) {
                Log.d("xxxx", oneOrder.getKey()
                        + " "
                        + eachRowInOrder.getDeliveryId()
                        + " "
                        + prefs.getUserId()
                        + eachRowInOrder.getStatus());
                if (eachRowInOrder.getDeliveryId() != prefs.getUserId()) {
                    mine = false;
                }
            }

            if (mine) {
                Log.d("xxxx","added : " + oneOrder.getKey() );
                myTasks.put(oneOrder.getKey(), oneOrder.getValue());
            }
        }

        return myTasks;
    }


    /*
    * get my tasks list by passing the allOrders
    * */
    public ArrayList<OrderDB> getMyTasksList(ArrayList<OrderDB> allTodayOrders, int myId) {

        ArrayList<OrderDB> myTasks = new ArrayList<>();

        for (OrderDB eachOrder : allTodayOrders) {
            if (eachOrder.getDeliveryId() == myId) {
                myTasks.add(eachOrder);
            }
        }
        return myTasks;
    }

    /*
    * get my tasks by passing the mytasks
    * */
    public HashMap<String, ArrayList<OrderDB>> getMyTasksDelivered(HashMap<String, ArrayList<OrderDB>> mytasks) {

        HashMap<String, ArrayList<OrderDB>> myTasksDelivered = new HashMap<>();

        for (Map.Entry<String, ArrayList<OrderDB>> oneOrder : mytasks.entrySet()) {
            ArrayList<OrderDB> customerOrdersList = oneOrder.getValue();
            boolean done = true;
            for (OrderDB eachRowInOrder : customerOrdersList) {
                if (!eachRowInOrder.getStatus().equals("delivered")) {
                    done = false;
                }
            }
            if (done) {
                myTasksDelivered.put(oneOrder.getKey(), oneOrder.getValue());
            }
        }

        return myTasksDelivered;
    }

    /*
    * get my tasks by passing the mytasks
    * */
    public HashMap<String, ArrayList<OrderDB>> getMyTasksNotDelivered(HashMap<String, ArrayList<OrderDB>> mytasks) {

        HashMap<String, ArrayList<OrderDB>> myTasksNotDelivered = new HashMap<>();

        for (Map.Entry<String, ArrayList<OrderDB>> oneOrder : mytasks.entrySet()) {
            ArrayList<OrderDB> customerOrdersList = oneOrder.getValue();
            boolean notdone = true;
            for (OrderDB eachRowInOrder : customerOrdersList) {
                if (eachRowInOrder.getStatus().equals("delivered")) {
                    notdone = false;
                }
            }
            if (notdone) {
                myTasksNotDelivered.put(oneOrder.getKey(), oneOrder.getValue());
            }
        }

        return myTasksNotDelivered;
    }

    /*
    * used for categorizing the orders based on the date
    * first key in hash map is customerId and the second key is date
    *
    * @param categorizedOrdersById
    * @return A hashmap that contains the orders and stored by the customerId and date
    * */

    public HashMap<Integer, HashMap<String, ArrayList<OrderDB>>> getCategorizedOrdersByCustomerIdAndDate(HashMap<Integer, ArrayList<OrderDB>> categorizedOrdersById) {

        HashMap<Integer, HashMap<String, ArrayList<OrderDB>>> finalmap = new HashMap<>();

        for (Map.Entry<Integer, ArrayList<OrderDB>> ordersBelongsToOneCustomerId : categorizedOrdersById.entrySet()) {

            ArrayList<OrderDB> ordersOfTheOneUser = ordersBelongsToOneCustomerId.getValue();

            HashMap<String, ArrayList<OrderDB>> ordersWithSameDate = new HashMap<>();

            for (OrderDB order : ordersOfTheOneUser) {
                // If the date does not exist in the hashmap
                if (!ordersWithSameDate.containsKey(order.getDate())) {
                    ArrayList<OrderDB> listInHash = new ArrayList<>();
                    listInHash.add(order);
                    ordersWithSameDate.put(order.getDate(), listInHash);
                } else {
                    // add the product to the arraylist that corresponds to the key
                    ArrayList<OrderDB> listInHash = ordersWithSameDate.get(order.getDate());
                    listInHash.add(order);
                }
            }
            finalmap.put(ordersBelongsToOneCustomerId.getKey(), ordersWithSameDate);
        }

        return finalmap;
    }

}
