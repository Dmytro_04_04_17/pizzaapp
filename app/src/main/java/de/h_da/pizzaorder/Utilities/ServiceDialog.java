package de.h_da.pizzaorder.Utilities;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;

import de.h_da.pizzaorder.R;

/**
 * Singleton for handling dialogs
 */
public class ServiceDialog {

    private static ServiceDialog serviceDialog;

    private AlertDialog dialog;

    private ServiceDialog() {
    }

    public static ServiceDialog getInstance() {
        if (serviceDialog == null) {
            serviceDialog = new ServiceDialog();
        }
        return serviceDialog;
    }

    public AlertDialog createLoader(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(R.layout.dialog_loader);
        this.dialog = builder.create();
        return dialog;
    }

    public AlertDialog createDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton(context.getString(R.string.Generall_Ok_Text), null);
        builder.setCancelable(false);
        return builder.show();
    }

    public void createDialog(Context context, @Nullable View view, String title, String message, String positiveText, DialogInterface.OnClickListener positive,
                             String negativeText, DialogInterface.OnClickListener negative,
                             boolean cancelable, boolean cancelableOutside) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if(view != null) {
            builder.setView(view);
        }

        builder.setTitle(title).setMessage(message).setPositiveButton(positiveText, positive).
                setNegativeButton(negativeText, negative);
        this.dialog = builder.create();
        this.dialog.setCanceledOnTouchOutside(cancelableOutside);
        this.dialog.setCancelable(cancelable);
        this.dialog.show();
    }

    public void hideDialog() {
        this.dialog.dismiss();
    }
}
