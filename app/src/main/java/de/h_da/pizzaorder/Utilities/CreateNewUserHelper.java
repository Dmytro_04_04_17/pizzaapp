package de.h_da.pizzaorder.Utilities;

import de.h_da.pizzaorder.Models.UserDB;

/**
 * Created by AEF
 */

/*
 * This class is used for create a new customer ( one user type )
 */
public class CreateNewUserHelper {
    /*
    * It gives us a create user for creating a customer
    * It is unique and is created at the initialisation of the system
    * The user types in system are : "admin , chef , customer , delivery and create"
    * For registering a new customer we need a limited user type which is create user
    * With it we get access just to users table in our database with permission only for inserting a new row
    * */
    public static UserDB getCreateUser(){
        return new UserDB("create", "create");
    }
}
