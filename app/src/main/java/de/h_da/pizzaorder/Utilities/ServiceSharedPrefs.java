package de.h_da.pizzaorder.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Singleton for handling SharedPreferences
 */
public class ServiceSharedPrefs {

    private static ServiceSharedPrefs sharedPrefs = null;

    private SharedPreferences sharedPreferences;

    private Editor edit;

    private static final String PREFERENCES_NAME = "de.h_da.pizzaorder.PREFERENCES_NAME";

    private static final String PREFERENCES_LOGIN = "de.h_da.pizzaorder.PREFERENCES_LOGIN";

    private static final String PREFERENCES_PASSWORD = "de.h_da.pizzaorder.PREFERENCES_PASSWORD";

    private static final String PREFERENCES_SAVE_DATA = "de.h_da.pizzaorder.PREFERENCES_SAVE_DATA";

    private static final String PREFERENCES_FIRST_LAUNCH = "de.h_da.pizzaorder.PREFERENCES_FIRST_LAUNCH";

    private static final String PREFERENCES_SUCCESSFUL_LOGIN = "de.h_da.pizzaorder.PREFERENCES_SUCCESSFUL_LOGIN";

    private static final String PREFERENCES_USER_ID = "de.h_da.pizzaorder.PREFERENCES_USER_ID";

    private static final String PREFERENCES_TOKEN = "de.h_da.pizzaorder.PREFERENCES_TOKEN";

    private static final String PREFERENCES_ADDRESS = "de.h_da.pizzaorder.PREFERENCES_ADDRESS";

    private static final String PREFERENCES_TELEPHONE = "de.h_da.pizzaorder.PREFERENCES_TELEPHONE";




    private ServiceSharedPrefs() {
    }

    public static ServiceSharedPrefs getInstance() {
        if (sharedPrefs == null) {
            sharedPrefs = new ServiceSharedPrefs();
        }
        return sharedPrefs;
    }

    public boolean checkFirstLaunch(Activity activity) {
        this.sharedPreferences = activity.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        this.edit = this.sharedPreferences.edit();
        return this.sharedPreferences.getBoolean(PREFERENCES_FIRST_LAUNCH, true);
    }

    public void setFirstLaucnch() {
        edit.putBoolean(PREFERENCES_FIRST_LAUNCH, false).apply();
    }

    public void saveData(String name, String password) {
        this.edit.putString(PREFERENCES_LOGIN, name);
        this.edit.putString(PREFERENCES_PASSWORD, password);
        this.edit.putBoolean(PREFERENCES_SAVE_DATA, true);
        this.edit.apply();
    }

    public boolean dataSaved() {
        return this.sharedPreferences.getBoolean(PREFERENCES_SAVE_DATA, false);
    }

    public String getLogin() {
        return this.sharedPreferences.getString(PREFERENCES_LOGIN, "");
    }

    public String getPassword() {
        return this.sharedPreferences.getString(PREFERENCES_PASSWORD, "");
    }

    public void deleteLogPass() {
        this.edit.putString(PREFERENCES_LOGIN, null);
        this.edit.putString(PREFERENCES_PASSWORD, null);
        this.edit.putBoolean(PREFERENCES_SAVE_DATA, false);
        this.edit.apply();
    }

    /**
     * When user loged successfully, save this and another time it will skip login and password
     */
    public void setSuccessfulLogin(boolean data) {
        this.edit.putBoolean(PREFERENCES_SUCCESSFUL_LOGIN, data).apply();
    }

    public boolean getSuccessfulLogin() {
        return this.sharedPreferences.getBoolean(PREFERENCES_SUCCESSFUL_LOGIN, false);
    }

    public void setUserId(int id) {
        this.edit.putInt(PREFERENCES_USER_ID, id).apply();
    }

    public int getUserId() {
        return this.sharedPreferences.getInt(PREFERENCES_USER_ID, 0);
    }

    public void setToken(String token) {
        this.edit.putString(PREFERENCES_TOKEN, token).apply();
    }

    public void setPassword(String password) {
        this.edit.putString(PREFERENCES_PASSWORD, password).apply();
    }

    public String getToken() {
        return this.sharedPreferences.getString(PREFERENCES_TOKEN, "");
    }

    public void saveAddressTelephone(String address, String telephone) {
        this.edit.putString(PREFERENCES_ADDRESS, address).putString(PREFERENCES_TELEPHONE, telephone).apply();
    }

    public String[] getAddressTelephone() {
        return new String[]{this.sharedPreferences.getString(PREFERENCES_ADDRESS, ""),
                this.sharedPreferences.getString(PREFERENCES_TELEPHONE, "") };
    }






}
