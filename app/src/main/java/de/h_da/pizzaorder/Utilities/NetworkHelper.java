package de.h_da.pizzaorder.Utilities;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by AEF
 */


/*
* This class is used to concentrate the data that is needed for network actions
* For example by changing the serverUrl in this class all other classes use this address for connecting
* */
public class NetworkHelper {

    private static NetworkHelper instance;

    private static int timeout = 1000;
    private static String serverUrl = "http://192.168.0.100/";

    private NetworkHelper() {
    }

    public static NetworkHelper getInstance() {
        if (instance == null) {
            instance = new NetworkHelper();
        }
        return instance;
    }

    public static boolean isServerReachable() {
        final boolean[] serverStatus = {false};
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    URL myUrl = new URL(serverUrl);
                    HttpURLConnection connection = (HttpURLConnection) myUrl.openConnection();
                    connection.setConnectTimeout(timeout);
                    connection.connect();
                    if (connection.getResponseCode() == HttpURLConnection.HTTP_OK)
                        serverStatus[0] = true;
                } catch (Exception e) {
                    serverStatus[0] = false;
                    e.printStackTrace();
                }
            }
        };

        Thread t = new Thread(runnable);
        t.start();
        while (t.isAlive()){
            //do nothing
        }
        return serverStatus[0];
    }

    public static String getServerUrl() {
        return serverUrl;
    }
}
