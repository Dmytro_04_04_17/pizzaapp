package de.h_da.pizzaorder.Utilities;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.CookieHandler;
import java.net.CookieManager;

import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Retrofit singleton for network requests
 */
public class RetrofitClient {

    public static String SERVER_URL = NetworkHelper.getServerUrl();

    private static String BASE_URL = SERVER_URL + "rest/main.php/";

    private static Retrofit retrofit = null;

    public static Retrofit getRetroftInstance() {
        if (retrofit == null) {

            HttpLoggingInterceptor logger = new HttpLoggingInterceptor();
            logger.setLevel(HttpLoggingInterceptor.Level.HEADERS);

            // init cookie manager
            CookieHandler cookieHandler = new CookieManager();

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            //Uncomment only in Debug mode
            //httpClient.addNetworkInterceptor(logger);
            httpClient.cookieJar(new JavaNetCookieJar(cookieHandler));

            Gson gson = new GsonBuilder().setLenient().create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build();
        }

        return retrofit;
    }
}




