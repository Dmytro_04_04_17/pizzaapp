package de.h_da.pizzaorder.Fragments.Delivery;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;

/**
 * A simple {@link Fragment} subclass.
 * When the user selects each Order this fragment will be loaded.
 * Most of the methods are look like {@link DeliveryMyTaskDetailsFragment}
 * Activities that contain this fragment must implement the
 * {@link DeliveryOrderDetailsFragment.Communicator} interface
 * to handle interaction events.
 * Use the {@link DeliveryOrderDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DeliveryOrderDetailsFragment extends Fragment implements View.OnClickListener {

    //String keys
    private static final String ITEMS_OF_THIS_ORDER = "items_of_this_order";
    private static final String MENUS_OF_THIS_ORDER = "menus_of_this_order";
    private static final String CUSTOMER_OF_THIS_ORDER = "customer_of_this_order";

    /*
    * UI elements
    * */
    private TextView tvOrderDateTime;
    private TextView tvOrderCustomerName;
    private TextView tvOrderCustomerAddress;
    private TextView tvOrderDetails;
    private Button bIWillDeliverIt;
    private Button bBackToOrdersList;


    // Parameters
    private ArrayList<OrderDB> mItemsOfThisOrder;
    private MenuDB[] mMenusOfThisOrder;
    private UserDB mCustomeOfThisOrder;
    private Communicator mComm;
    private Context context;
    private RetrofitInterface retrofit = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);
    private ServiceSharedPrefs prefs = ServiceSharedPrefs.getInstance();


    public DeliveryOrderDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param itemsofthisorder    each order can have multiple items .
     * @param menusofthisorder    items contain menu ids and this variable holds the menus of these ids
     * @param customerofthisorder type is UserDb and holds the data of the orders customer
     * @return A new instance of fragment DeliveryOrderDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DeliveryOrderDetailsFragment newInstance(ArrayList<OrderDB> itemsofthisorder
            , MenuDB[] menusofthisorder
            , UserDB customerofthisorder) {
        DeliveryOrderDetailsFragment fragment = new DeliveryOrderDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ITEMS_OF_THIS_ORDER, itemsofthisorder);
        args.putParcelableArray(MENUS_OF_THIS_ORDER, menusofthisorder);
        args.putParcelable(CUSTOMER_OF_THIS_ORDER, customerofthisorder);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItemsOfThisOrder = getArguments().getParcelableArrayList(ITEMS_OF_THIS_ORDER);
            mMenusOfThisOrder = (MenuDB[]) getArguments().getParcelableArray(MENUS_OF_THIS_ORDER);
            mCustomeOfThisOrder = getArguments().getParcelable(CUSTOMER_OF_THIS_ORDER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delivery_order_details, container, false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            mComm = (Communicator) context;
            this.context = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DeliveryOrderDetailsFragmentCommunicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mComm = null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeUI(view);
        setData();

    }


    private void initializeUI(View view) {
        this.bBackToOrdersList = (Button) view.findViewById(R.id.fButtonDeliveryBackToOrdersList);
        bBackToOrdersList.setOnClickListener(this);

        this.bIWillDeliverIt = (Button) view.findViewById(R.id.fButtonDeliveryIWillDeliverIt);
        bIWillDeliverIt.setOnClickListener(this);

        this.tvOrderDateTime = (TextView) view.findViewById(R.id.fTextViewDeliveryOrderDateTime);
        this.tvOrderCustomerName = (TextView) view.findViewById(R.id.fTextViewDeliveryCustomerName);
        this.tvOrderCustomerAddress = (TextView) view.findViewById(R.id.fTextViewDeliveryCustomerAddress);
        this.tvOrderDetails = (TextView) view.findViewById(R.id.fTextViewDeliveryOrderDetails);
    }

    private void setData() {

        /* TODO : All data should be check not be null */
        this.tvOrderDateTime.append(mItemsOfThisOrder.get(0).getDate());
        this.tvOrderCustomerName.append(mCustomeOfThisOrder.getName() + " " + mCustomeOfThisOrder.getFamily());
        this.tvOrderCustomerAddress.append(" " + mItemsOfThisOrder.get(0).getCustomerAddress());

        String orderDetails = "\nThis order contains : " + String.valueOf(mItemsOfThisOrder.size()) + " item\n";
        orderDetails += "\nCustomer telephone : " + mItemsOfThisOrder.get(0).getCustomerTelephone() + "\n";
        orderDetails += "Customer selected Meal : \n";
        for (MenuDB menu : mMenusOfThisOrder) {
            orderDetails += menu.getName() + "\n";
        }

        this.tvOrderDetails.append(orderDetails);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fButtonDeliveryIWillDeliverIt:
                if (mComm != null) {
                    /*
                    * create ordersid array
                    * */
                    int[] ordersid = new int[mItemsOfThisOrder.size()];

                    for (int i = 0; i < mItemsOfThisOrder.size(); i++) {
                        ordersid[i] = mItemsOfThisOrder.get(i).getId();
                    }
                    mComm.respondIWillDeliverItButton(ordersid);

                }
                break;
            case R.id.fButtonDeliveryBackToOrdersList:
                mComm.respondBackToOrderList();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface Communicator {
        void respondIWillDeliverItButton(int[] ordersId);

        void respondBackToOrderList();


    }
}
