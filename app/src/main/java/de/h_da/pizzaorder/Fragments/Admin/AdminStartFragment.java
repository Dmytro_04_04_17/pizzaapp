package de.h_da.pizzaorder.Fragments.Admin;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Start Fragment for making a choice
 */
public class AdminStartFragment extends Fragment {

    public static final String USERS = "Users";

    public static final  String MENUS = "Menu";

    public static final  String ORDERS = "Orders";

    public static final String ADD = "Add";

    public static final String CHANGE = "Change";

    private final List<String> choiceData = Arrays.asList(USERS, MENUS, ORDERS);

    private final List<String> actionData = Arrays.asList(ADD, CHANGE);

    private Spinner choiceSpinner;

    private Spinner actionSpinner;

    private Button okButton;

    private final RetrofitInterface retrofit = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);

    private final ServiceDialog serviceDialog = ServiceDialog.getInstance();

    private final ServiceSharedPrefs prefs = ServiceSharedPrefs.getInstance();

    private Communicator  comm;

    public AdminStartFragment() {

    }

    public static AdminStartFragment newInstance() {
        return new AdminStartFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_start, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initializeUI(view);
        this.setData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator ) {
            comm = (Communicator ) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        comm = null;
    }

    /**
     * Initializes widgets
     * @param view - current view
     */
    private void initializeUI(View view) {
        this.choiceSpinner = (Spinner) view.findViewById(R.id.AdminFragmentStart_Choice_Spinner);
        this.actionSpinner = (Spinner) view.findViewById(R.id.AdminFragmentStart_Action_Spinner);
        this.okButton = (Button) view.findViewById(R.id.AdminFragmentStart_Ok_Button);
    }

    /**
     * Sets data to widgets
     */
    private void setData() {
        ArrayAdapter<String> choiceAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, this.choiceData);
        ArrayAdapter<String> actionAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, this.actionData);
        choiceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        actionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.actionSpinner.setAdapter(actionAdapter);
        this.choiceSpinner.setAdapter(choiceAdapter);

        this.okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String choice = choiceData.get(choiceSpinner.getSelectedItemPosition());
                String action = actionData.get(actionSpinner.getSelectedItemPosition());
                makeRequest(choice, action);
            }
        });

    }

    /**
     * Determines which request to do
     * @param choice - chosen type (UserDB, MenuDB, OrderDB)
     * @param action - chosen action (Create New or Change existing)
     */
    private void makeRequest(String choice, String action) {
        if (action.equals(AdminStartFragment.ADD)) {
            if(!choice.equals(ORDERS)) {
                this.comm.respondAdminStartFragmentCreateNew(this.choiceSpinner.getSelectedItemPosition());
            } else {
                this.serviceDialog.createDialog(getActivity(), getActivity().getString(R.string.admin_fragment_start_permissions));
            }
        } else {
            this.serviceDialog.createLoader(getActivity()).show();
            if (choice.equals(AdminStartFragment.USERS)) {
                this.makeRequetUsers();
            } else if (choice.equals(AdminStartFragment.MENUS)) {
                this.makeRequetMenus();
            } else {
                this.makeRequetOrders();
            }
        }
    }

    /**
     * Makes specific requets for User type
     */
    private void makeRequetUsers() {
        Call<Map<String, List<UserDB>>> call = this.retrofit.getAllUsers(this.prefs.getToken());
        call.enqueue(new Callback<Map<String, List<UserDB>>>() {
            @Override
            public void onResponse(Call<Map<String, List<UserDB>>> call, Response<Map<String, List<UserDB>>> response) {
                serviceDialog.hideDialog();
                comm.respondAdminStartFragmentShowFragment(0, response.body().get(AdminStartFragment.USERS.toLowerCase()));
            }

            @Override
            public void onFailure(Call<Map<String, List<UserDB>>> call, Throwable t) {
                showErrorDialog(t.getMessage());
            }
        });
    }

    /**
     * Makes specific requets for Menu type
     */
    private void makeRequetMenus() {
        Call<Map<String, List<MenuDB>>> call = this.retrofit.getCurrentMenu(this.prefs.getToken());
        call.enqueue(new Callback<Map<String, List<MenuDB>>>() {
            @Override
            public void onResponse(Call<Map<String, List<MenuDB>>> call, Response<Map<String, List<MenuDB>>> response) {
                serviceDialog.hideDialog();
                comm.respondAdminStartFragmentShowFragment(1,response.body().get(AdminStartFragment.MENUS.toLowerCase()));
            }

            @Override
            public void onFailure(Call<Map<String, List<MenuDB>>> call, Throwable t) {
                showErrorDialog(t.getMessage());
            }
        });
    }

    /**
     * Makes specific requets for Order type
     */
    private void makeRequetOrders() {
        Call<Map<String, List<OrderDB>>> call = this.retrofit.getAllOrders(this.prefs.getToken());
        call.enqueue(new Callback<Map<String, List<OrderDB>>>() {
            @Override
            public void onResponse(Call<Map<String, List<OrderDB>>> call, Response<Map<String, List<OrderDB>>> response) {
                serviceDialog.hideDialog();
                comm.respondAdminStartFragmentShowFragment(2, response.body().get(AdminStartFragment.ORDERS.toLowerCase()));
            }

            @Override
            public void onFailure(Call<Map<String, List<OrderDB>>> call, Throwable t) {
                showErrorDialog(t.getMessage());
            }
        });
    }

    /**
     * Show error dialog
     * @param message - message to show
     */
    private void showErrorDialog(String message) {
        this.serviceDialog.hideDialog();
        this.serviceDialog.createDialog(getActivity(), message);
    }

    public interface Communicator {
        void respondAdminStartFragmentShowFragment(int data, List list);
        void respondAdminStartFragmentCreateNew(int data);
    }
}
