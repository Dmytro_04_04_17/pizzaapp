package de.h_da.pizzaorder.Fragments.Admin;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import de.h_da.pizzaorder.Adapters.Admin.AdminShowAdapter;
import de.h_da.pizzaorder.R;

/**
 * Fragment which shows the information about data chosen
 */
public class AdminShowFragment extends Fragment {
    // For identifiying saved params which were passed
    private static final String ARG_PARAM1 = "param1";

    private static final String ARG_PARAM2 = "param2";

    // data passed (can be UserDB, MenuDB or OrderDB
    private List listData;

    // type passed (0 - UserDB, 1 - MenuDB, 2 - OrderDB)
    private int type;

    // an instance to an Activity
    private Communicator comm;

    private AdminShowAdapter adminShowAdapter;

    private RecyclerView recyclerView;

    private Button backButton;

    public AdminShowFragment() {
    }

    public static AdminShowFragment newInstance(int type, ArrayList data) {
        AdminShowFragment fragment = new AdminShowFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, data);
        args.putInt(ARG_PARAM2, type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.type = getArguments().getInt(ARG_PARAM2);
            this.listData = getArguments().getParcelableArrayList(ARG_PARAM1);
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_admin_show, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.backButton = (Button)view.findViewById(R.id.AdminShowFragment_Back_Button);
        this.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comm.respondAdminShowFragmentBack();
            }
        });
        this.recyclerView = (RecyclerView)view.findViewById(R.id.AdminShowFragment_RecyclerView);
        this.adminShowAdapter = new AdminShowAdapter(getActivity(), this.type, this.listData);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.recyclerView.setAdapter(this.adminShowAdapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            comm = (Communicator) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        comm = null;
    }


    public interface Communicator {
        // TODO: Update argument type and name
        void respondAdminShowFragmentBack();
    }
}
