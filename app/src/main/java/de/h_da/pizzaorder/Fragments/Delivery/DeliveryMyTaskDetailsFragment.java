package de.h_da.pizzaorder.Fragments.Delivery;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;

/**
 * A simple {@link Fragment} subclass.
 * When the user selects each Task this fragment will be loaded
 * Activities that contain this fragment must implement the
 * {@link Communicator} interface
 * to handle interaction events.
 * Use the {@link DeliveryMyTaskDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DeliveryMyTaskDetailsFragment extends Fragment implements View.OnClickListener {

    //String keys
    private static final String ITEMS_OF_THIS_TASK = "items_of_this_task";
    private static final String MENUS_OF_THIS_TASK = "menus_of_this_task";
    private static final String CUSTOMER_OF_THIS_TASK = "customer_of_this_task";

    /*
    * UI elements
    * */
    private TextView tvTaskDateTime;
    private TextView tvTaskCustomerName;
    private TextView tvTaskCustomerAddress;
    private TextView tvTaskDetails;
    private Button bDelivered;
    private Button bBackToMyTasks;

    // Parameters
    private ArrayList<OrderDB> mItemsOfThisTask;
    private MenuDB[] mMenusOfThisTask;
    private UserDB mCustomerOfThisTask;
    private Communicator mComm;
    private Context context;

    public DeliveryMyTaskDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param itemsofthistask    Parameter 1.
     * @param menusofthistask    Parameter 2.
     * @param customerofthistask Parameter 3.
     * @return A new instance of fragment DeliveryMyTaskDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DeliveryMyTaskDetailsFragment newInstance(ArrayList<OrderDB> itemsofthistask
            , MenuDB[] menusofthistask
            , UserDB customerofthistask) {
        DeliveryMyTaskDetailsFragment fragment = new DeliveryMyTaskDetailsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ITEMS_OF_THIS_TASK, itemsofthistask);
        args.putParcelableArray(MENUS_OF_THIS_TASK, menusofthistask);
        args.putParcelable(CUSTOMER_OF_THIS_TASK, customerofthistask);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItemsOfThisTask = getArguments().getParcelableArrayList(ITEMS_OF_THIS_TASK);
            mMenusOfThisTask = (MenuDB[]) getArguments().getParcelableArray(MENUS_OF_THIS_TASK);
            mCustomerOfThisTask = getArguments().getParcelable(CUSTOMER_OF_THIS_TASK);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delivery_my_task_details, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            mComm = (Communicator) context;
            this.context = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DeliveryMyTaskDetailsFragmentCommunicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mComm = null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeUI(view);
        setData();
    }

    /*
    * Whenever an DeliveryMyTaskDetailsFragment is created this function is called
    * to initialize the UI variables
    * @param view is the current view which contains the UI variables
    * */
    private void initializeUI(View view) {
        this.bBackToMyTasks = (Button) view.findViewById(R.id.fButtonDeliveryMyTaskList);
        bBackToMyTasks.setOnClickListener(this);

        this.bDelivered = (Button) view.findViewById(R.id.fButtonDeliveryMyTaskDelivered);
        bDelivered.setOnClickListener(this);

        this.tvTaskDateTime = (TextView) view.findViewById(R.id.fTextViewDeliveryMyTaskOrderDateTime);
        this.tvTaskCustomerName = (TextView) view.findViewById(R.id.fTextViewDeliveryMytaskCustomerName);
        this.tvTaskCustomerAddress = (TextView) view.findViewById(R.id.fTextViewDeliveryMyTaskCustomerAddress);
        this.tvTaskDetails = (TextView) view.findViewById(R.id.fTextViewDeliveryMyTaskOrderDetails);
    }

    /*
    * Set the data to the UI elements based on the data that is caught from selected Task
    * Here we can customise the way data would be displayed to the user
    * */
    private void setData() {

        /* TODO : All data should be check not be null */
        this.tvTaskDateTime.append(mItemsOfThisTask.get(0).getDate());
        this.tvTaskCustomerName.append(mCustomerOfThisTask.getName() + " " + mCustomerOfThisTask.getFamily());
        this.tvTaskCustomerAddress.append(mItemsOfThisTask.get(0).getCustomerAddress());

        String taskDetails = "Customer telephone : " + mItemsOfThisTask.get(0).getCustomerTelephone() + "\n\n";
        taskDetails +=   "This order contains : " + String.valueOf(mItemsOfThisTask.size()) + " item\n";
        taskDetails += "Customer selected Meal : \n";
        for (MenuDB menu : mMenusOfThisTask) {
            taskDetails += menu.getName() + "\n";
        }

        this.tvTaskDetails.append(taskDetails);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fButtonDeliveryMyTaskDelivered:
                if (mComm != null) {
                    /*
                    * create ordersId array
                    * */
                    int[] ordersid = new int[mItemsOfThisTask.size()];

                    for (int i = 0; i < mItemsOfThisTask.size(); i++) {
                        ordersid[i] = mItemsOfThisTask.get(i).getId();
                    }
                    mComm.respondDeliveredButton(ordersid);
                }
                break;
            case R.id.fButtonDeliveryMyTaskList:
                mComm.respondBackToMyTasksButton();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface Communicator {
        /*
        * When the delivery user deliver an order, he will click the button delivered.
        * Here we need ids of the orders that are delivered to change their status
        * @param ordersId It contains the ids of the orders that are delivered
        * */
        void respondDeliveredButton(int[] ordersId);

        void respondBackToMyTasksButton();
    }
}
