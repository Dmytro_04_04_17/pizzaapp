package de.h_da.pizzaorder.Fragments.Customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.RetrofitClient;

public class CustomerFragmentDescription extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";

    private static final String ARG_PARAM2 = "param2";

    public static final String preImage = RetrofitClient.SERVER_URL + "images/";

    public static final String postImage = ".png";

    private boolean previewMode;

    private MenuDB menu;

    private Communicator comm;

    private TextView name;

    private TextView ingredients;

    private TextView weight;

    private TextView price;

    private ImageView image;

    private Button addButton;

    private Button backButton;

    private Button orderButton;

    public CustomerFragmentDescription() {
    }

    public static CustomerFragmentDescription newInstance(MenuDB menu, boolean preview) {
        CustomerFragmentDescription fragment = new CustomerFragmentDescription();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, menu);
        args.putBoolean(ARG_PARAM2, preview);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.menu = getArguments().getParcelable(ARG_PARAM1);
            this.previewMode = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customer_description, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initializeUI(view);
        this.setData();
        this.setListeners();
        this.checkMode();
    }

    /**
     * Sets listeners
     */
    private void setListeners() {
        this.addButton.setOnClickListener(this);
        this.backButton.setOnClickListener(this);
        this.orderButton.setOnClickListener(this);
    }

    /**
     * Sets data
     */
    private void setData() {
        this.name.setText(this.menu.getName());
        this.ingredients.setText(this.menu.getIngredients());
        this.weight.setText(String.valueOf(this.menu.getWeight()));
        this.price.setText(String.valueOf(this.menu.getPrice()));
        String temp = preImage + this.menu.getImage() + postImage;
        Glide.with(getActivity()).load(temp).error(R.drawable.error).into(this.image);
    }

    /**
     * Initializes widgets
     * @param view - current view
     */
    private void initializeUI(View view) {
        this.name = (TextView) view.findViewById(R.id.FragmentCustomerDescription_Name_TextView);
        this.ingredients = (TextView) view.findViewById(R.id.FragmentCustomerDescription_Ingredients_TextView);
        this.weight = (TextView) view.findViewById(R.id.FragmentCustomerDescription_Weight_TextView);
        this.price = (TextView) view.findViewById(R.id.FragmentCustomerDescription_Price_TextView);
        this.image = (ImageView) view.findViewById(R.id.FragmentCustomerDescription_Image_ImageView);
        this.addButton = (Button) view.findViewById(R.id.FragmentCustomerDescription_Add_Button);
        this.backButton = (Button) view.findViewById(R.id.FragmentCustomerDescription_Back_Button);
        this.orderButton = (Button) view.findViewById(R.id.FragmentCustomerDescription_Order_Button);
    }

    /**
     * Checks in which mode the user is
     */
    private void checkMode() {
        if (this.previewMode) {
            this.addButton.setVisibility(View.GONE);
            this.orderButton.setVisibility(View.GONE);
        }
        if(!((BaseActivity)comm).getPortraitMode()) {
            this.backButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            comm = (Communicator) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Communicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        comm = null;
    }

    @Override
    public void onClick(View v) {
        if(comm != null) {
            switch(v.getId()) {
                case R.id.FragmentCustomerDescription_Add_Button:
                    comm.respondCustomerFragmentDescriptionAdd(menu);
                    break;
                case R.id.FragmentCustomerDescription_Back_Button:
                    comm.respondCustomerFragmentDescriptionBack();
                    break;
                case R.id.FragmentCustomerDescription_Order_Button:
                    comm.respondCustomerFragmentDescriptionOrder(this.menu);
                    break;
                default:
                    throw new IllegalArgumentException("Invalid choice");
            }
        }

    }

    public interface Communicator {
        void respondCustomerFragmentDescriptionBack();

        void respondCustomerFragmentDescriptionAdd(MenuDB menu);

        void respondCustomerFragmentDescriptionOrder(MenuDB menu);
    }
}
