package de.h_da.pizzaorder.Fragments.Chef;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.h_da.pizzaorder.Adapters.Chef.ChefAllAdapter;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.R;

public class ChefAllOrdersFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";

    private static final String ARG_PARAM2 = "param2";

    private List<MenuDB> menuList;

    private Communicator comm;

    private RecyclerView recyclerView;

    private Button confirmButton;

    private Button cancelButton;

    private TextView titleTextView;

    private boolean ordersToCook;

    public ChefAllOrdersFragment() {

    }

    public static ChefAllOrdersFragment newInstance(ArrayList<MenuDB> param1, boolean param2) {
        ChefAllOrdersFragment fragment = new ChefAllOrdersFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.menuList = getArguments().getParcelableArrayList(ARG_PARAM1);
            this.ordersToCook = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chef_all_orders, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.recyclerView = (RecyclerView)view.findViewById(R.id.FragmentChefAllOrder_RecyclerView);
        this.confirmButton = (Button)view.findViewById(R.id.FragmentCheffAllOrders_Cook_Button);
        this.cancelButton = (Button)view.findViewById(R.id.FragmentCheffAllOrders_Cancel_Button);
        this.titleTextView = (TextView) view.findViewById(R.id.FragmentChefAllOrder_Title_TextView);
        this.setData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            this.comm = (Communicator) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.comm = null;
    }

    /**
     * Sets data for widgets
     */
    private void setData() {
        if (this.ordersToCook) {
            this.titleTextView.setText(getString(R.string.chef_current_orders));
            this.confirmButton.setText(getString(R.string.chef_start_cook));
            this.cancelButton.setVisibility(View.GONE);
        } else {
            this.titleTextView.setText(getString(R.string.chef_own_orders));
            this.confirmButton.setText(getString(R.string.chef_finish_cook));
            this.cancelButton.setVisibility(View.VISIBLE);
            this.cancelButton.setText(getString(R.string.Generall_Cancel_Text));
        }

        this.recyclerView.setAdapter(new ChefAllAdapter(this.menuList, getActivity()));
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.confirmButton.setOnClickListener(this);
        this.cancelButton.setOnClickListener(this);
        if(this.menuList.size() == 0) {
            this.confirmButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.FragmentCheffAllOrders_Cook_Button:
                if(this.comm != null) {
                    if (this.ordersToCook) {
                        this.comm.respondAllOrders(true);
                    } else {
                        this.comm.respondAllOrders(false);
                    }
                }
                break;
            case R.id.FragmentCheffAllOrders_Cancel_Button:
                this.comm.respondAllOrdersCancel();
                break;
        }
    }

    public interface Communicator {
        void respondAllOrders(boolean forCooking);
        void respondAllOrdersCancel();
    }
}
