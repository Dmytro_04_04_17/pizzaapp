package de.h_da.pizzaorder.Fragments.Delivery;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.h_da.pizzaorder.R;

/**
 * A simple {@link Fragment} subclass.
 * This fragment shows the statistics of the orders and user tasks
 *
 * TODAY ORDERS : shows all the today orders with all the statuses
 * READY FOR DELIVERY : shows all the orders that are ready with deliveryId = 0
 * YOUR TASKS : shows all the orders that have the specific deliveryId with status delivered and cooked
 * (cooked here means order is going to delivered)
 * DELIVERED BY YOU : shows all the tasks that are done.
 * Difference between OUR TASKS and DELIVERED BY YOU shows that user has some tasks to do and also equal this number
 * we have rows in our tasks list
 * Activities that contain this fragment must implement the
 * {@link DeliveryStatisticsFragment.Communicator} interface
 * to handle interaction events.
 * Use the {@link DeliveryStatisticsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DeliveryStatisticsFragment extends Fragment{

    // the fragment initialization parameters
    private static final String TODAY_ORDERS = "today_orders";
    private static final String READY_FOR_DELIVERY = "ready_for_delivery";
    private static final String YOUR_TASKS = "your_tasks";
    private static final String DELIVERED_BY_YOU = "delivered_by_you";

    // Statistics params
    private String mTodayOrders;
    private String mReadyForDelivery;
    private String mYourTasks;
    private String mDeliveredByYou;

    private Communicator mComm;

    //UI elements
    private TextView tvDeliveryTodayOrders;
    private TextView tvDeliveryReadyForDelivery;
    private TextView tvDeliveryNumberOFYourTasks;
    private TextView tvDeliveryDeliveredByYou;

    public DeliveryStatisticsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param todayOrders      Parameter 1.
     * @param readyForDelivery Parameter 2.
     * @param yourTasks        Parameter 3.
     * @param deliveredByYou   Parameter 4.
     * @return A new instance of fragment DeliveryStatisticsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DeliveryStatisticsFragment newInstance(int todayOrders, int readyForDelivery, int yourTasks, int deliveredByYou) {
        DeliveryStatisticsFragment fragment = new DeliveryStatisticsFragment();
        Bundle args = new Bundle();
        args.putString(TODAY_ORDERS, String.valueOf(todayOrders));
        args.putString(READY_FOR_DELIVERY, String.valueOf(readyForDelivery));
        args.putString(YOUR_TASKS, String.valueOf(yourTasks));
        args.putString(DELIVERED_BY_YOU, String.valueOf(deliveredByYou));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTodayOrders = getArguments().getString(TODAY_ORDERS);
            mReadyForDelivery = getArguments().getString(READY_FOR_DELIVERY);
            mYourTasks = getArguments().getString(YOUR_TASKS);
            mDeliveredByYou = getArguments().getString(DELIVERED_BY_YOU);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delivery_statistics, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initializeUI(view);

        tvDeliveryTodayOrders.append(mTodayOrders);
        tvDeliveryReadyForDelivery.append(mReadyForDelivery);
        tvDeliveryNumberOFYourTasks.append(mYourTasks);
        tvDeliveryDeliveredByYou.append(mDeliveredByYou);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator
                ) {
            mComm = (Communicator) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DeliveryStatisticsFragmentCommunicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mComm = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface Communicator {

    }


    private void initializeUI(View view) {
        this.tvDeliveryTodayOrders = (TextView) view.findViewById(R.id.fTextViewDeliveryTodayOrders);
        this.tvDeliveryReadyForDelivery = (TextView) view.findViewById(R.id.fTextViewDeliveryReadyForDelivery);
        this.tvDeliveryNumberOFYourTasks = (TextView) view.findViewById(R.id.fTextViewDeliveryNumberOFYourTasks);
        this.tvDeliveryDeliveredByYou = (TextView) view.findViewById(R.id.fTextViewDeliveryDeliveredByYou);
    }

}
