package de.h_da.pizzaorder.Fragments.Customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Adapters.Customer.ChangeAdapter;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.R;

public class CustomerChangeFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private List<MenuDB> userList;

    private List<Boolean> checkboxList;

    private RecyclerView recyclerView;

    private ChangeAdapter changeAdapter;

    private Communicator comm;

    private Button okButton;

    public CustomerChangeFragment() {

    }


    public static CustomerChangeFragment newInstance(ArrayList<MenuDB> list) {
        CustomerChangeFragment fragment = new CustomerChangeFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, list);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.userList = getArguments().getParcelableArrayList(ARG_PARAM1);
            this.checkboxList = new ArrayList<>(Collections.nCopies(this.userList.size(), false));
            this.changeAdapter = new ChangeAdapter(this.userList, this.checkboxList);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initializeUI(view);
        this.setData();
        this.setListeners();
    }

    /**
     * Sets listeners for widgets
     */
    private void setListeners() {
        this.okButton.setOnClickListener(this);
    }

    /**
     * Sets data
     */
    private void setData() {
        this.recyclerView.setAdapter(this.changeAdapter);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    /**
     * Initializes widgets
     * @param view - current view
     */
    private void initializeUI(View view) {
        this.okButton = (Button) view.findViewById(R.id.CustomerChangeFragment_Ok_Button);
        this.recyclerView = (RecyclerView) view.findViewById(R.id.CustomerChangeFragment_RecyclerView);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            comm = (Communicator) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Communicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        comm = null;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.CustomerChangeFragment_Ok_Button:
                if (comm != null) {
                    this.updateList();
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value");
        }
    }

    /**
     * Updates data in this fragment
     */
    private void updateList() {
        int i=0;
        List<MenuDB> tempList = new ArrayList<>();
        for (boolean data: this.checkboxList) {
            if(!data) {
                tempList.add(this.userList.get(i));
            }
            i++;
        }
        comm.respondCustomerChangeFragmentRefresh(tempList);

        if(!((BaseActivity)comm).getPortraitMode()) {
            this.userList.clear();
            this.checkboxList.clear();
            if(tempList.size() != 0) {
                this.userList.addAll(tempList);
                this.checkboxList.addAll(new ArrayList<>(Collections.nCopies(this.userList.size(), false)));
            }
            this.changeAdapter.notifyDataSetChanged();

            if(this.checkboxList.size() == 0) {
                this.okButton.setVisibility(View.GONE);
            }
        }
    }

    public interface Communicator {
        // TODO: Update argument type and name
        void respondCustomerChangeFragmentRefresh(List<MenuDB> list);
    }
}
