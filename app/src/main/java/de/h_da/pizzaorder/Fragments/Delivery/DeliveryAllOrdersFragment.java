package de.h_da.pizzaorder.Fragments.Delivery;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import de.h_da.pizzaorder.Adapters.Delivery.AllOrdersAdapter;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;

/**
 * A simple {@link Fragment} subclass.
 * It shows all the orders with status "Ready" that don't belong to any delivery persons
 * status = "ready"
 * deliveryId = 0
 * Activities that contain this fragment must implement the
 * {@link DeliveryAllOrdersFragment.Communicator} interface
 * to handle interaction events.
 * Use the {@link DeliveryAllOrdersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DeliveryAllOrdersFragment extends Fragment implements View.OnClickListener {

    // the fragment initialization parameters
    private static final String TODAY_READY_ORDERS_WITHOUT_DELIVERY_ID = "today_ready_orders_without_delivery_id";

    //parameters
    /*
    * this map contains only the ready orders with deliveryId = 0
    * KEY : is the time of the order and it's format is DATETIME
    * */
    private HashMap<String, ArrayList<OrderDB>> mTodayReadyOrdersWhithoutDeliveryId;


    private Context context;
    //UI elements

    /*All the ready orders will be loaded in this RecyclerView*/
    private RecyclerView rvAllorders;
    private LinearLayoutManager llmRecyclerManager;
    /*This adapter is used for creating the each row of the RecyclerView*/
    private AllOrdersAdapter aAllOrdersAdapter;
    /*Button for showing the MyTasksFragment*/
    private Button bShowMyTasks;
    /*Button for refreshing the data which is loaded in the list*/
    private Button bRefreshOrdersList;
    private Communicator mComm;


    public DeliveryAllOrdersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param todayReadyOrdersWithoutDeliveryId Parameter 1.
     * @return A new instance of fragment DeliveryAllOrdersFragment.
     */
    public static DeliveryAllOrdersFragment newInstance(HashMap<String, ArrayList<OrderDB>> todayReadyOrdersWithoutDeliveryId) {
        DeliveryAllOrdersFragment fragment = new DeliveryAllOrdersFragment();
        Bundle args = new Bundle();
        args.putSerializable(TODAY_READY_ORDERS_WITHOUT_DELIVERY_ID, todayReadyOrdersWithoutDeliveryId);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.mTodayReadyOrdersWhithoutDeliveryId =
                    (HashMap<String, ArrayList<OrderDB>>) getArguments().getSerializable(TODAY_READY_ORDERS_WITHOUT_DELIVERY_ID);
            aAllOrdersAdapter = new AllOrdersAdapter(mTodayReadyOrdersWhithoutDeliveryId, context);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delivery_all_orders, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeUI(view);
        setData();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            mComm = (Communicator) context;
            this.context = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DeliveryAllOrdersFragmentCommunicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mComm = null;
    }

    /*
    * Whenever an AllOrdersFragment is created this function is called
    * to initialize the UI variables
    * @param view is the current view which contains the UI variables
    * */
    private void initializeUI(View view) {
        this.bShowMyTasks = (Button) view.findViewById(R.id.fButtonDeliveryShowMyTasks);
        bShowMyTasks.setOnClickListener(this);

        this.bRefreshOrdersList = (Button) view.findViewById(R.id.fButtonDeliveryRefreshOrdersList);
        bRefreshOrdersList.setOnClickListener(this);
        /* RecyclerView initialization */
        rvAllorders = (RecyclerView) view.findViewById(R.id.fRecyclerViewDeliveryFragmentAllOrders);

    }

    private void setData() {
        /*
        * It is a routine for using a RecyclerView
        * Always we need an adapter and LayoutManager that should be set for the RecyclerView
        * */
        rvAllorders.setAdapter(aAllOrdersAdapter);
        llmRecyclerManager = new LinearLayoutManager(context);
        rvAllorders.setLayoutManager(llmRecyclerManager);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fButtonDeliveryShowMyTasks:
                mComm.respondShowMyTaskListButton();
                break;

            case R.id.fButtonDeliveryRefreshOrdersList:
                mComm.respondRefreshOrdersListButton();
                break;
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface Communicator {
        void respondShowMyTaskListButton();

        void respondRefreshOrdersListButton();
    }

}
