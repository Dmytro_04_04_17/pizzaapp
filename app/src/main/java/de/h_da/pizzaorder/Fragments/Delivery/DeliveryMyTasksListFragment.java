package de.h_da.pizzaorder.Fragments.Delivery;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.HashMap;

import de.h_da.pizzaorder.Adapters.Delivery.MyTasksAdapter;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.OrdersHelper;

/**
 * A simple {@link Fragment} subclass.
 * It shows all the tasks of the delivery user.
 * Most of the methods are look like the {@link DeliveryAllOrdersFragment}
 * Activities that contain this fragment must implement the
 * {@link Communicator} interface
 * to handle interaction events.
 * Use the {@link DeliveryMyTasksListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DeliveryMyTasksListFragment extends Fragment implements View.OnClickListener {
    //String Keys
    private static final String MY_TASKS_LIST = "my_tasks_list";


    private HashMap<String, ArrayList<OrderDB>> mMyTasksCategorizedByDateTime;

    private Communicator mComm;

    private Context context;

    private OrdersHelper ordersHelper = OrdersHelper.getInstance();


    //UI elements
    private RecyclerView rvAllMyTasks;
    private LinearLayoutManager llmRecyclerManager;
    private MyTasksAdapter aMyTasksAdapter;
    private Button bShowAllOrders;

    public DeliveryMyTasksListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param mytaskslist Parameter 1.
     * @return A new instance of fragment DeliveryMyTasksListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DeliveryMyTasksListFragment newInstance(HashMap<String,ArrayList<OrderDB>> mytaskslist) {
        DeliveryMyTasksListFragment fragment = new DeliveryMyTasksListFragment();
        Bundle args = new Bundle();
        args.putSerializable(MY_TASKS_LIST, mytaskslist);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMyTasksCategorizedByDateTime = ordersHelper.getMyTasksNotDelivered(
                    (HashMap<String, ArrayList<OrderDB>>) getArguments().getSerializable(MY_TASKS_LIST)
            );
            aMyTasksAdapter = new MyTasksAdapter(mMyTasksCategorizedByDateTime,context);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delivery_my_tasks_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeUI(view);
        setData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            mComm = (Communicator) context;
            this.context = context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Communicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mComm = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fButtonDeliveryShowAllOrdersList:
                mComm.respondShowOrdersList();
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface Communicator {
        void respondShowOrdersList();
    }


    private void initializeUI(View view) {
        this.bShowAllOrders = (Button) view.findViewById(R.id.fButtonDeliveryShowAllOrdersList);
        bShowAllOrders.setOnClickListener(this);
        /* RecyclerView initialisation */
        rvAllMyTasks = (RecyclerView) view.findViewById(R.id.DeliveryFragmentMyTasksList_RecyclerView);
    }

    private void setData() {
        rvAllMyTasks.setAdapter(aMyTasksAdapter);
        llmRecyclerManager = new LinearLayoutManager(context);
        rvAllMyTasks.setLayoutManager(llmRecyclerManager);
    }
}
