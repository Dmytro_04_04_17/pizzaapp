package de.h_da.pizzaorder.Fragments.Customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Adapters.Customer.MenuAdapter;

public class CustomerFragmentMain extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView recyclerView;

    private Button orderButton;

    private Button changeButton;

    private TextView numberTextView;

    private int ordersCurrent;

    private boolean needUpdate;

    private List<MenuDB> list;

    private List<MenuDB> userList;

    private MenuAdapter menuAdapter;

    private Communicator comm;

    public CustomerFragmentMain() {
    }

    public static CustomerFragmentMain newInstance(ArrayList<MenuDB> menu, ArrayList<MenuDB> userMenu) {
        CustomerFragmentMain fragment = new CustomerFragmentMain();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM1, (menu));
        args.putParcelableArrayList(ARG_PARAM2, (userMenu));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.list = getArguments().getParcelableArrayList(ARG_PARAM1);
            this.userList = getArguments().getParcelableArrayList(ARG_PARAM2);
            this.menuAdapter = new MenuAdapter(this.list, getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initializeUI(view);
        this.setData();
        this.setListeners();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(this.needUpdate) {
            this.numberTextView.setText(String.format(getString(R.string.CustomFragmentMain_CurrentOrders), ordersCurrent));
            this.needUpdate = false;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            this.comm = (Communicator) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Communicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.comm = null;
    }

    /**
     * Initializes widgets from xml
     * @param view - current view
     */
    private void initializeUI(View view) {
        this.orderButton = (Button) view.findViewById(R.id.CustomerFragment_Order_Button);
        this.changeButton = (Button) view.findViewById(R.id.CustomerFragment_Change_Button);
        this.numberTextView = (TextView) view.findViewById(R.id.CustomerFragment_Number_TextView);
        this.recyclerView = (RecyclerView)view.findViewById(R.id.CustomerFragment_RecyclerView);
    }

    /**
     * Sets data
     */
    private void setData() {
        this.recyclerView.setAdapter(menuAdapter);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.numberTextView.setText(String.format(getString(R.string.CustomFragmentMain_CurrentOrders), ordersCurrent));
    }

    /**
     * Sets listeners
     */
    private void setListeners() {
        this.orderButton.setOnClickListener(this);
        this.changeButton.setOnClickListener(this);
    }

    /**
     * Updates the number displayed in the food in a basket
     * @param value - value to be written
     */
    public void updateNumber(int value) {
            this.needUpdate = true;
            this.ordersCurrent = value;
        if(comm != null && !((BaseActivity) comm).getPortraitMode()) {
            this.numberTextView.setText(String.format(getString(R.string.CustomFragmentMain_CurrentOrders), ordersCurrent));
        }
    }

    /**
     * Resets the number for a basket
     */
    public void resetNumber() {
        this.numberTextView.setText(String.format(getString(R.string.CustomFragmentMain_CurrentOrders), 0));
    }

    /**
     * Handles changes in a data
     * @param list - list with a new data
     */
    public void notifyFragmentData(List<MenuDB> list) {
        this.list.clear();
        if(list.size() > 0) {
            this.list.addAll(list);
        }
        this.menuAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        if (this.comm != null) {
            switch(v.getId()) {
                case R.id.CustomerFragment_Order_Button:
                    comm.respondCustomerFragmentMainMakeOrder();
                    break;
                case R.id.CustomerFragment_Change_Button:
                    comm.respondCustomerFragmentMainChangeFragment();
                    break;
                default:
                    throw new IllegalStateException("Unexpected value");
            }
        }
    }

    public interface Communicator {
        void respondCustomerFragmentMainMakeOrder();
        void respondCustomerFragmentMainChangeFragment();
    }
}
