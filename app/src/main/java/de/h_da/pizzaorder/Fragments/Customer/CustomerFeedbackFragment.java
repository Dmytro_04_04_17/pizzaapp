package de.h_da.pizzaorder.Fragments.Customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import de.h_da.pizzaorder.Activities.Custom.BaseActivity;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerFeedbackFragment extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM1 = "param1";

    private static final String ARG_PARAM2 = "param2";

    private OrderDB orderDB;

    private boolean previewMode;

    private EditText editText;

    private TextView feedbackTextView;

    private RatingBar ratingBar;

    private Button sendButton;

    private Button backButton;

    private Communicator comm;

    private RetrofitInterface retrofit = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);

    private ServiceDialog dialog = ServiceDialog.getInstance();

    public CustomerFeedbackFragment() {
    }

    public static CustomerFeedbackFragment newInstance(OrderDB param1, boolean param2) {
        CustomerFeedbackFragment fragment = new CustomerFeedbackFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        args.putBoolean(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.orderDB = getArguments().getParcelable(ARG_PARAM1);
            this.previewMode = getArguments().getBoolean(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_customer_feedback, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.initializeUI(view);
        this.setListeners();
        this.setData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            comm = (Communicator) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement Communicator");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        comm = null;
    }

    /**
     * Initializes widgets
     * @param view - current view
     */
    private void initializeUI(View view) {
        this.editText = (EditText) view.findViewById(R.id.FragmentCustomerFeedback_EditText);
        this.ratingBar = (RatingBar) view.findViewById(R.id.FragmentCustomerFeedback_RatingBar);
        this.sendButton = (Button) view.findViewById(R.id.FragmentCustomerFeedback_Send_Button);
        this.backButton = (Button) view.findViewById(R.id.FragmentCustomerFeedback_Back_Button);
        this.feedbackTextView = (TextView) view.findViewById(R.id.FragmentCustomerFeedback_Feedback_TextView);
    }

    /**
     * Sets listeners
     */
    private void setListeners() {
        this.editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    sendButton.performClick();
                    return true;
                }
                return false;
            }
        });

        this.sendButton.setOnClickListener(this);
        this.backButton.setOnClickListener(this);
    }

    /**
     * Sets data
     */
    private void setData() {
        if (this.previewMode) {
            String feedback = this.orderDB.getCustomerFeedback();
            this.feedbackTextView.setText(feedback.substring(1, feedback.length()));
            this.ratingBar.setIsIndicator(true);
            try {
                int rating = Integer.parseInt(feedback.substring(0,1));
                this.ratingBar.setRating(rating);
            } catch (Exception e) {
                Log.d("TAG", "Exception thrown");
            }
            this.editText.setVisibility(View.GONE);
            this.sendButton.setVisibility(View.GONE);
        } else {
            this.feedbackTextView.setVisibility(View.GONE);
            this.ratingBar.setIsIndicator(false);
        }

        if(!((BaseActivity)comm).getPortraitMode()) {
            this.backButton.setVisibility(View.GONE);
        }
    }

    /**
     * Sends request with a feedback filled
     */
    private void sendFeedback() {
        Call<Integer> call = retrofit.sendFeedback(this.orderDB.getId(), ServiceSharedPrefs.getInstance().getToken(),
                ((int)this.ratingBar.getRating()) + this.editText.getText().toString());
        call.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                dialog.createDialog(CustomerFeedbackFragment.this.getActivity(), getString(R.string.dialog_order_complete_title));
                comm.respondCustomerFeedbackSendButton();
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                dialog.createDialog(CustomerFeedbackFragment.this.getActivity(), t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.FragmentCustomerFeedback_Send_Button:
                InputMethodManager imm = (InputMethodManager)CustomerFeedbackFragment.this.
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                if(editText.getText().length() > 0) {
                    sendFeedback();
                } else {
                    dialog.createDialog(CustomerFeedbackFragment.this.getActivity(), getString(R.string.feedback_empty));
                }
                break;
            case R.id.FragmentCustomerFeedback_Back_Button:
                comm.respondCustomerFeedbackBackButton();
                break;
        }
    }


    public interface Communicator {
        // TODO: Update argument type and name
        void respondCustomerFeedbackSendButton();
        void respondCustomerFeedbackBackButton();
    }
}
