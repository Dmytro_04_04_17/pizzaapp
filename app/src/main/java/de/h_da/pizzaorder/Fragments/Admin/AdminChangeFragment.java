package de.h_da.pizzaorder.Fragments.Admin;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;

import de.h_da.pizzaorder.Fragments.Customer.CustomerFragmentDescription;
import de.h_da.pizzaorder.Models.ImageDb;
import de.h_da.pizzaorder.Models.MenuDB;
import de.h_da.pizzaorder.Models.OrderDB;
import de.h_da.pizzaorder.Models.UserDB;
import de.h_da.pizzaorder.R;
import de.h_da.pizzaorder.Utilities.RetrofitClient;
import de.h_da.pizzaorder.Utilities.RetrofitInterface;
import de.h_da.pizzaorder.Utilities.ServiceDialog;
import de.h_da.pizzaorder.Utilities.ServiceSharedPrefs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Fragment for changing data or creating a new
 */
public class AdminChangeFragment extends Fragment implements View.OnClickListener {
    // For identifiying saved params which were passed
    private static final String ARG_PARAM1 = "param1";

    private static final String ARG_PARAM2 = "param2";

    // data passed (can be UserDB, MenuDB or OrderDB
    private Parcelable data;

    // determines whether this fragment should be used for creating a new value or show existing
    private boolean createNew;

    // type passed (0 - UserDB, 1 - MenuDB, 2 - OrderDB)
    private int type;

    // an instance to an Activity
    private Communicator comm;

    // For displating the id
    private TextView idTextView;

    // Used when layout for UserDB invoked

    private EditText loginEditText;

    private EditText passEditText;

    private Spinner typeSpinner;

    private final List<String> spinnerData = Arrays.asList("customer", "chef", "delivery", "admin");

    // Used when layout for MenuDB invoked

    private EditText nameEditText;

    private EditText descriptionEditText;

    private EditText weightEditText;

    private EditText priceEditText;

    private ImageView imageView;

    private boolean imageUploaded;

    // Used when layout for OrderDB invoked

    private TextView customerIdTextView;

    private TextView menuIdTextView;

    private TextView chefIdTextView;

    private TextView deliveryIdTextView;

    private TextView statusTextView;

    private TextView feedbackTextView;

    private TextView addressTextView;

    private TextView telephoneTextView;

    private TextView dateTextView;

    // Shared views and instances

    private Button backButton;

    private Button okButton;

    private Button addImageButton;

    private Switch activeSwitch;

    private final RetrofitInterface retrofit = RetrofitClient.getRetroftInstance().create(RetrofitInterface.class);

    private final ServiceDialog serviceDialog = ServiceDialog.getInstance();

    private final ServiceSharedPrefs prefs = ServiceSharedPrefs.getInstance();

    public AdminChangeFragment() {
    }

    public static AdminChangeFragment newInstance(@Nullable Integer type, @Nullable Parcelable data) {
        AdminChangeFragment fragment = new AdminChangeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, type);
        if (data != null) {
            args.putParcelable(ARG_PARAM2, data);
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.type = getArguments().getInt(ARG_PARAM1);
            this.data = getArguments().getParcelable(ARG_PARAM2);
            if (this.data == null) {
                this.createNew = true;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int resource;
        switch(this.type) {
            case 0:
                resource = R.layout.fragment_adminchange_user;
                break;
            case 1:
                resource = R.layout.fragment_adminchange_menu;
                break;
            case 2:
                resource = R.layout.fragment_adminchange_order;
                break;
            default:
                throw new IllegalStateException("Unexpected value");
        }
        return inflater.inflate(resource, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        switch(this.type) {
            case 0:
                this.initializeUIUser(view);
                this.setListenersUser();
                this.setDataUser();
                break;
            case 1:
                this.initializeUIMenu(view);
                this.setListenersMenu();
                this.setDataMenu();
                break;
            case 2:
                this.initializeUIOrder(view);
                this.setListenersOrder();
                this.setDataOrder();
                break;
            default:
                throw new IllegalStateException("Unexpected value");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Communicator) {
            this.comm = (Communicator) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.comm = null;
    }

    /**
     * Initializes views
     * @param view - current view
     */
    private void initializeUIUser(View view) {
        this.typeSpinner = (Spinner) view.findViewById(R.id.AdminChange_Type_Spinner);
        this.idTextView = (TextView) view.findViewById(R.id.AdminChange_ID_TextView);
        this.loginEditText = (EditText) view.findViewById(R.id.AdminChange_Login_EditText);
        this.passEditText = (EditText) view.findViewById(R.id.AdminChange_Pass_EditText);
        this.backButton = (Button)view.findViewById(R.id.AdminChangeMenu_Cancel_Button);
        this.okButton = (Button)view.findViewById(R.id.AdminChangeMenu_Ok_Button);
    }

    /**
     * Sets listeners
     */
    private void setListenersUser() {
        this.backButton.setOnClickListener(this);
        this.okButton.setOnClickListener(this);
        this.passEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE)) {
                    hideKeyboard();
                    okButton.performClick();
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Sets Data to components used
     */
    private void setDataUser() {
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, this.spinnerData);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.typeSpinner.setAdapter(adapter);

        if(!this.createNew) {
            this.idTextView.setText(String.valueOf(((UserDB)data).getId()));
            this.loginEditText.setText(((UserDB)data).getUsername());
            this.passEditText.setText(((UserDB)data).getPassword());

            String type = ((UserDB)this.data).getType();

            for (int i=0; i<this.spinnerData.size(); i++) {
                if (this.spinnerData.get(i).equals(type)) {
                    this.typeSpinner.setSelection(i);
                }
            }
        }
    }

    /**
     * Checks whether required fields are filled for User type
     * @return - return true if filled, otherwise false
     */
    private boolean allInfoFilledUser() {
        String login = this.loginEditText.getText().toString();
        String pass = this.passEditText.getText().toString();
        String type = this.spinnerData.get(this.typeSpinner.getSelectedItemPosition());
        if (login.length() > 0 && pass.length() > 0 ) {
            if(this.createNew) {
                data = new UserDB(login, pass, type);
            } else {
                ((UserDB)this.data).setUsername(login);
                ((UserDB)this.data).setPassword(pass);
                ((UserDB)this.data).setType(type);
            }
            this.hideKeyboard();
            return true;
        }
        return false;
    }

    /**
     * Handles OK button click for User type
     */
    private void handleOkUser() {
        if (this.allInfoFilledUser()) {
            this.handleSendUser((UserDB)data, this.createNew);
        }
    }

    /**
     * Initializes views
     * @param view - current view
     */
    private void initializeUIMenu(View view) {
        this.nameEditText = (EditText) view.findViewById(R.id.AdminChangeMenu_Name_EditText);
        this.descriptionEditText = (EditText) view.findViewById(R.id.AdminChangeMenu_Description_EditText);
        this.weightEditText = (EditText) view.findViewById(R.id.AdminChangeMenu_Weight_EditText);
        this.priceEditText = (EditText) view.findViewById(R.id.AdminChangeMenu_Price_EditText);
        this.backButton = (Button)view.findViewById(R.id.AdminChangeMenu_Cancel_Button);
        this.okButton = (Button)view.findViewById(R.id.AdminChangeMenu_Ok_Button);
        this.addImageButton = (Button)view.findViewById(R.id.AdminChangeMenu_Image_Button);
        this.activeSwitch = (Switch) view.findViewById(R.id.AdminChangeMenu_Active_Switch);
        this.idTextView = (TextView) view.findViewById(R.id.AdminChangeMenu_ID_TextView);
        this.imageView = (ImageView) view.findViewById(R.id.ADminChange_ImageView);
    }

    /**
     * Sets listeners
     */
    private void setListenersMenu() {
        this.backButton.setOnClickListener(this);
        this.okButton.setOnClickListener(this);
        this.addImageButton.setOnClickListener(this);
        this.priceEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_NEXT)) {
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * Sets Data to components used
     */
    private void setDataMenu() {
        if (!this.createNew) {
            this.idTextView.setText(String.valueOf(((MenuDB)data).getId()));
            this.nameEditText.setText(((MenuDB)data).getName());
            this.descriptionEditText.setText(((MenuDB)data).getIngredients());
            this.weightEditText.setText(String.valueOf(((MenuDB)data).getWeight()));
            this.priceEditText.setText(String.valueOf(((MenuDB)data).getPrice()));
            String imagePath = CustomerFragmentDescription.preImage + ((MenuDB)data).getImage() + CustomerFragmentDescription.postImage;

            Glide.with(getActivity()).load(imagePath).error(R.drawable.error).override(200,200).fitCenter().into(this.imageView);
            if (((MenuDB)data).getActive() == 1) {
                this.activeSwitch.setChecked(true);
            }
        }
    }

    /**
     * Checks whether required fields are filled for Menu type
     * @return - return true if filled, otherwise false
     */
    private boolean allInfoFilledMenu(){
        String name = this.nameEditText.getText().toString();
        String description = this.descriptionEditText.getText().toString();
        String weight = this.weightEditText.getText().toString();
        String price = this.priceEditText.getText().toString();
        if (name.length() > 0 && description.length() > 0 && weight.length() > 0 && price.length() > 0 ) {
            this.hideKeyboard();
            String image = "default";
            if (this.imageUploaded) {
                image = name;
            }
            int active = 0;
            if (this.activeSwitch.isChecked()) {
                active = 1;
            }
            if(this.createNew) {
                this.data = new MenuDB(name, description, Double.valueOf(weight), Double.valueOf(price), image, active);
            } else {
                ((MenuDB)this.data).setName(name);
                ((MenuDB)this.data).setIngredients(description);
                ((MenuDB)this.data).setWeight(Double.valueOf(weight));
                ((MenuDB)this.data).setPrice(Double.valueOf(price));
                ((MenuDB)this.data).setImage(image);
                ((MenuDB)this.data).setActive(active);
            }
            return true;
        }
        return false;
    }

    /**
     * Handles OK button click for Menu type
     */
    private void handleOkMenu() {
        if (this.allInfoFilledMenu()) {
            this.handleSendMenu((MenuDB)data, this.createNew);
        }
    }

    /**
     * Initializes views
     * @param view - current view
     */
    private void initializeUIOrder(View view) {
        this.idTextView = (TextView) view.findViewById(R.id.AdminChange_ID_TextView);
        this.customerIdTextView = (TextView) view.findViewById(R.id.AdminChange_CustomerId_TextView);
        this.menuIdTextView = (TextView) view.findViewById(R.id.AdminChange_MenuId_TextView);
        this.chefIdTextView = (TextView) view.findViewById(R.id.AdminChange_ChefId_TextView);
        this.deliveryIdTextView = (TextView) view.findViewById(R.id.AdminChange_DeliveryId_TextView);
        this.statusTextView = (TextView) view.findViewById(R.id.AdminChange_Status_TextView);
        this.feedbackTextView = (TextView) view.findViewById(R.id.AdminChange_Feedback_TextView);
        this.addressTextView = (TextView) view.findViewById(R.id.AdminChange_Address_TextView);
        this.telephoneTextView = (TextView) view.findViewById(R.id.AdminChange_Telephone_TextView);
        this.dateTextView = (TextView) view.findViewById(R.id.AdminChange_Date_TextView);
        this.backButton = (Button)view.findViewById(R.id.AdminChangeMenu_Cancel_Button);
        this.okButton = (Button)view.findViewById(R.id.AdminChangeMenu_Ok_Button);
    }

    /**
     * Sets listeners
     */
    private void setListenersOrder() {
        this.backButton.setOnClickListener(this);
        this.okButton.setOnClickListener(this);
    }

    /**
     * Sets Data to components used
     */
    private void setDataOrder() {
        if(!this.createNew) {
            this.idTextView.setText(String.valueOf(((OrderDB)data).getId()));
            this.customerIdTextView.setText(String.valueOf(((OrderDB)data).getCustomerId()));
            this.menuIdTextView.setText(String.valueOf(((OrderDB)data).getMenuId()));
            this.chefIdTextView.setText(String.valueOf(((OrderDB)data).getChefId()));
            this.deliveryIdTextView.setText(String.valueOf(((OrderDB)data).getDeliveryId()));
            this.statusTextView.setText(((OrderDB)data).getStatus());
            this.feedbackTextView.setText(((OrderDB)data).getCustomerFeedback());
            this.addressTextView.setText(((OrderDB)data).getCustomerAddress());
            this.telephoneTextView.setText(((OrderDB)data).getCustomerTelephone());
            this.dateTextView.setText(((OrderDB)data).getDate().substring(11));
            this.okButton.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Because the possibility to create a new order is Off, always the same value for using the same structure
     * @return - true
     */
    private boolean allInfoFilledOrder() {
        return true;
    }

    /**
     * Handles OK button click for Order type
     */
    private void handleOkOrder() {
        if (this.allInfoFilledOrder()) {
            this.handleSendOrder((OrderDB) data, this.createNew);
        }
    }

    /**
     * Hides keyboard
     */
    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.backButton.getWindowToken(), 0);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.AdminChangeMenu_Cancel_Button:
                comm.respondAdminChangeFragmentBack();
                break;
            case R.id.AdminChangeMenu_Ok_Button:
                switch(this.type) {
                    case 0:
                        this.handleOkUser();
                        break;
                    case 1:
                        this.handleOkMenu();
                        break;
                    case 2:
                        this.handleOkOrder();
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value");
                }
                break;
            case R.id.AdminChangeMenu_Image_Button:
                comm.respondAdminChangeFragmentImage();
                break;
            default:
                throw new IllegalStateException("Unexpected value");
        }
    }

    /**
     * Send info for User type
     * @param user - the data to be sent
     * @param createNew - if true creates a new, false - changes existing
     */
    private void handleSendUser(UserDB user, boolean createNew) {
        this.serviceDialog.createLoader(getActivity());
        if(createNew) {
            Call<Integer> call = this.retrofit.createUser(this.prefs.getToken(), user);
            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    serviceDialog.hideDialog();
                    comm.respondAdminChangeFragmentSend();
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    serviceDialog.hideDialog();
                    serviceDialog.createDialog(getActivity(), t.getMessage());
                }
            });
        } else {
            Call<Integer> call = this.retrofit.changeUser(String.valueOf(user.getId()), this.prefs.getToken(), user);
            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    serviceDialog.hideDialog();
                    comm.respondAdminChangeFragmentSend();
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    serviceDialog.hideDialog();
                    serviceDialog.createDialog(getActivity(), t.getMessage());
                }
            });
        }
    }

    /**
     * Send info for Menu type
     * @param menu - the data to be sent
     * @param createNew - if true creates a new, false - changes existing
     */
    private void handleSendMenu(MenuDB menu, boolean createNew) {
        this.serviceDialog.createLoader(getActivity());
        if(createNew) {
            Call<Integer> call = this.retrofit.createMenu(this.prefs.getToken(), menu);
            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    serviceDialog.hideDialog();
                    comm.respondAdminChangeFragmentSend();
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    serviceDialog.hideDialog();
                    serviceDialog.createDialog(getActivity(), t.getMessage());
                }
            });
        } else {
            Call<Integer> call = this.retrofit.changeMenu(String.valueOf(menu.getId()), this.prefs.getToken(), menu);
            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    serviceDialog.hideDialog();
                    comm.respondAdminChangeFragmentSend();
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    serviceDialog.hideDialog();
                    serviceDialog.createDialog(getActivity(), t.getMessage());
                }
            });
        }
    }

    /**
     * Send info for Order type
     * @param order - the data to be sent
     * @param createNew - true, just to follow the scheme like a USer and Menu types
     */
    private void handleSendOrder(OrderDB order, boolean createNew) {
        this.serviceDialog.createLoader(getActivity());
        if(createNew) {
            Call<Integer> call = this.retrofit.createOrder(this.prefs.getToken(), order);
            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    serviceDialog.hideDialog();
                    comm.respondAdminChangeFragmentSend();
                }

                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    serviceDialog.hideDialog();
                    serviceDialog.createDialog(getActivity(), t.getMessage());
                }
            });
        } else {
            order.getId();
            //TODO Update Info
        }
    }

    /**
     * Sets image chosen to a view
     * @param bitmap - chosen image
     */
    public void setImage(Bitmap bitmap) {
        if(this.nameEditText.getText().toString().length() != 0) {
            Bitmap resizedbitmap1 = Bitmap.createScaledBitmap(bitmap, 200, 200, true);
            this.imageView.setImageBitmap(resizedbitmap1);
            this.uploadImage(bitmap);
        } else {
            this.serviceDialog.createDialog(getActivity(), getActivity().getString(R.string.admin_change_fragment_name_for_image));
        }
    }

    /**
     * Uploads the image to a server
     * @param bitmap - chosen image
     */
    private void uploadImage(Bitmap bitmap) {
        String title = this.nameEditText.getText().toString().toLowerCase();
        String image = imageToString(bitmap);

        Call<ImageDb> call = this.retrofit.uploadImage(true, prefs.getToken(), prefs.getLogin(), title, image);
        this.serviceDialog.createLoader(getActivity());
        call.enqueue(new Callback<ImageDb>() {
            @Override
            public void onResponse(Call<ImageDb> call, Response<ImageDb> response) {
                    // you can use the path to store the location of the image
                if (response.code() == 200) {
                   imageUploaded = true;
                }
                serviceDialog.hideDialog();

            }

            @Override
            public void onFailure(Call<ImageDb> call, Throwable t) {
                serviceDialog.hideDialog();
            }
        });
    }

    /**
     * Converts image for sending
     * @param bitmap - chosen image
     * @return - converted image
     */
    private String imageToString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 20, byteArrayOutputStream);
        byte[] imgBytes = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgBytes, Base64.DEFAULT);
    }

    public interface Communicator {
        void respondAdminChangeFragmentSend();
        void respondAdminChangeFragmentBack();
        void respondAdminChangeFragmentImage();
    }
}
